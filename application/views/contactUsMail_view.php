<!DOCTYPE html>
<html>
<head>
	<title>Contact US</title>
	<style>
table, th, td {
  border: 1px solid black;
}
</style>
</head>
<body>
	<table>
		<thead>
			<tr>
				<th>Name</th>
				<th>Email</th>
			</tr>
			<tr>
				<td><?= $name?></td>
				<td><?= $email?></td>
			</tr>
			<tr><td colspan="4" style="text-align:center"><?= $subject?></td></tr>
			<tr><td colspan="4" rowspan="4" style="text-align:center"><?= $message?></td></tr>
		</thead>
	</table>

</body>
</html>