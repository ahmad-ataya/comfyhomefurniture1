<section class="main-header" style="padding-top: 20px;">
    <header>
        <div class="container">
            <h1 class="h2 title"><?=$product->{'title_'.$langCode}?></h1>
            <ol class="breadcrumb breadcrumb-inverted">
                <li><a href="<?=base_url()?>"><span class="icon icon-home"></span></a></li>
                <li><a  href="<?=base_url()?>category/<?=$product->categoryRoute?>"><?=$product->{'category_title_'.$langCode}?></a></li>
                <li><a class="active"  href="<?=base_url()?>products/<?=$product->id?>"><?=$product->{'title_'.$langCode}?></a></li>
            </ol>
        </div>
    </header>
</section>


<section class="product" style="padding-bottom: 0px;">
    <div class="main">
        <div class="container">
            <div class="row product-flex">

                <!-- product flex is used only for mobile order -->
                <!-- on mobile 'product-flex-info' goes bellow gallery 'product-flex-gallery' -->

                <div class="col-md-4 col-sm-12 product-flex-info">
                    <div class="clearfix">

                        <!-- === product-title === -->

                        <h1 class="title" data-title="<?=$product->{'title_'.$langCode}?>"><?=$product->{'title_'.$langCode}?></small></h1>

                        <div class="clearfix">

                            <!-- === price wrapper === -->

                            <div class="price">
                                <span class="h3">
                                    $ <?= $product->price - $product->discount;?>
                                    <?php if($product->discount > 0 ){ ?>
                                        <small>$ <?= $product->price;?></small>
                                    <?php } ?>
                                </span>
                            </div>
                            <hr />

                            <!-- === info-box === -->

                            <!-- <div class="info-box">
                                <span><strong>Maifacturer</strong></span>
                                <span>Brand name</span>
                            </div> -->

                            <!-- === info-box === -->
                            <?php if($product->material != ''){ ?>
                            
                                <div class="info-box">
                                    <span><strong>Materials</strong></span>
                                    <span><?= $product->material;?></span>
                                </div>
                            <?php } ?>
                            <!-- === info-box === -->

                            <div class="info-box">
                                <span><strong>Availability</strong></span>
                                <?php if($product->in_stock == 1) { ?>
                                    <span><i class="fa fa-check-square-o"></i> In stock</span>
                                <?php }else if($product->in_stock == 2){ ?>
                                    <span><i class="fa fa-truck"></i> Due back in stock <br/><span style="color: red;"><?=$product->due_stock;?></span></span>
                                <?php }else{ ?>
                                    <span><i class="fa fa-truck"></i> Out of stock</span>
                                <?php } ?>
                            </div>

                            <hr />

                            <!-- === info-box === -->

                            <?php if($product->size != ''){ ?>
                                <div class="info-box">
                                    <span><strong>Size</strong></span>
                                    <div class="product-colors clearfix">
                                        <span class=""><?= $product->size;?></span>
                                    </div>
                                </div>
                            <?php } ?>


                            <?php if($product->weight != ''){ ?>
                                <div class="info-box">
                                    <span><strong>Weight</strong></span>
                                    <div class="product-colors clearfix">
                                        <span class=""><?= $product->weight;?></span>
                                    </div>
                                </div>
                            <?php } ?>
                            <?php if($product->color != ''){ ?>
                                <?php 
                                    try {
                                        $product->color = json_decode($product->color);
                                    } catch (Exception $e) {
                                        $product->color = array();
                                        
                                    }
                                ?>
                                <?php if(count($product->color) > 0){ ?>
                                    <div class="info-box">
                                        <span><strong>Available Colors</strong></span>
                                        <div class="product-colors clearfix">
                                            <?php foreach($product->color as $color){ ?>
                                                <span class="color-btn" style="border-radius: 5rem; background-color: <?= $color; ?>"></span>
                                            <?php } ?>
                                        </div>
                                    </div>
                                <?php }?>
                            <?php } ?>

                            <hr>


                            <div class="info-box">
                                <span><?= str_replace("\n",'<br/>',$product->{'desc_'.$langCode})?> </span>
                            </div> 

                        </div> <!--/clearfix-->
                    </div> <!--/product-info-wrapper-->
                </div> <!--/col-md-4-->
                <!-- === product item gallery === -->

                <div class="col-md-8 col-sm-12 product-flex-gallery">
                    <!-- === product gallery === -->

                    <div class="owl-product-gallery open-popup-gallery">
                        <?php if(count($images) == 0){ ?>
                            <a href="<?=base_url()?>assets/images/site/products/<?=$product->image?>"><img src="<?=base_url()?>assets/images/site/products/<?=$product->image?>" height="500" /></a>
                        
                        <?php } else{ foreach ($images as $image) { ?>
                            <a href="<?=base_url()?>assets/images/site/products/<?=$image->image?>"><img src="<?=base_url()?>assets/images/site/products/<?=$image->image?>" height="500" /></a>
                        <?php }}?>
                    </div>
                </div>

            </div>
        </div>
    </div>

</section>


<?php $this->load->view('site/footer_internal_pages'); ?>
