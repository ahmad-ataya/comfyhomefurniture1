<div class="col-md-3 col-sm-3 col-xs-12" style="margin-bottom: 10px;">
    <article>
        <div class="info">
              <!--   <span class="add-favorite added">
                    <a href="javascript:void(0);" data-title="Add to favorites" data-title-added="Added to favorites list"><i class="icon icon-heart"></i></a>
                </span> -->
            <span>
                <a href="<?=base_url()?>products/<?=($product->route != '')?$product->route:$product->id?>"  data-title="Quick wiew"><i class="icon icon-eye"></i></a>
            </span>
        </div>
      
        <div class="figure-grid">
            <div class="image">
                <a href="<?=base_url()?>products/<?=($product->route != '')?$product->route:$product->id?>" >
                    <img src="<?=base_url()?>assets/images/site/products/<?=$product->image?>" alt="" width="360" height="<?=($isTwoProduct)?200:200?>" />
                </a>
            </div>
            <div class="text row" style="margin-left:0px;margin-right: 0px; margin-top: 10px;">
                    
                <div class="col-sm-6 col-xs-6" style="padding: 0;">
                    <h2 class="title h5"><a href="<?=base_url()?>products/<?=($product->route != '')?$product->route:$product->id?>"><?=$product->{'title_'.$langCode}?></a></h2>
                </div>
                <div class="col-sm-6 col-xm-6 price" style="padding: 0;" >
                    <?php if($product->price == 0){ ?>
                        <span class="h3" style="color: red;float: right;font-size: small;">
                            <a href="tel:7802337841">
                                <img src="<?=base_url()?>assets/images/site/callMe.png" style="top: -8px; height: 30px; width: 30px;">Call Me
                            </a>
                        </span>
                    <?php } else{ ?>
                        <span class="h3" style="color: red;float: right;font-size: larger;">
                            <?php if($product->discount > 0 ){ ?>
                                <sub><small style="    font-size: small;">$ <?= $product->price;?></small></sub>
                            <?php } ?>
                            $ <?= $product->price - $product->discount;?>
                        </span>
                    <?php } ?>
                   
                </div>
            </div>

            <div class="text row" style="margin-left:0px;margin-right: 0px; margin-top: -12px; padding-top: 0px">
                    
                <div class="col-sm-6 col-xs-6" style="height: 40px;margin-top: -10px;">
                    <span style="font-size:14px;"><?=$product->{'short_desc_'.$langCode}?></span>
                </div>
                <div class="col-sm-6 col-xs-6" >
                    <?php if($product->in_stock == 1){ ?> 
                        <span style="float: right; color: green;font-size: 14px;">In Stock</span>
                    <?php }else if($product->in_stock == 2){ ?> 
                        <span style="float: right; color: green;font-size: 14px; margin-top: -15px;">Due back in stock <span style="color: red;"><?=$product->due_stock;?></span></span>
                    <?php }else{ ?>
                        <span style="float: right; color: green;font-size: 14px;">Out Of Stock</span>
                    <?php } ?> 
                </div>
            </div>

        </div>
    </article>
</div>