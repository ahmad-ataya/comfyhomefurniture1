 <!-- ========================  Header content ======================== -->

        <section class="header-content">

            <div class="owl-slider">
                <!-- === slide item === -->
                <?php  foreach ($sliders as $slider){ ?>
                    <div class="item" style="background-image:url(<?=base_url()?>assets/images/site/slider/<?=$slider->image?>)">
                        <div class="box">
                            <div class="container">
                                <h2 class="title animated h1" data-animation="fadeInDown"><?= $slider->{'title_'.$langCode}?></h2>
                                <div class="animated" data-animation="fadeInUp">
                                    <?= $slider->description;?>
                                </div>
                                <?php if($slider->link != ''){ ?>
                                    <div class="animated" data-animation="fadeInUp">
                                        <a href="<?=base_url().$slider->link?>" class="btn btn-clean">More Details</a>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                <?php }?>
            </div> <!--/owl-slider-->
        </section>

        <!-- ========================  Icons slider ======================== -->
<!--
        <section class="owl-icons-wrapper owl-icons-frontpage">


            <header class="hidden">
                <h2>Product categories</h2>
            </header>

            <div class="container">

                <div class="owl-icons">

                    <?php foreach ($categories as $category){ ?>
                        <a href="<?=base_url()?>category/<?=$category->route;?>">
                            <figure>
                                <i class="f-icon <?=($category->icon)?$category->icon:'f-icon-sofa'?>"></i>
                                <figcaption><?=$category->{'title_'.$langCode}?></figcaption>
                            </figure>
                        </a>
                    <?php } ?>

                </div> 
            </div> 
        </section>
-->
        <!-- ========================  Products widget ======================== -->

        <section class="products">

            <div class="" style="margin-left: 2%;margin-right: 2%; padding-right: 1%;padding-left: 1%; ">

                <!-- === header title === -->

                <header>
                    <div class="row">
                        <div class="col-md-offset-2 col-md-8 text-center">
                            <h2 class="title lineUnderText" >SOFAS</h2>
                            <!-- <div class="text"> -->
                                <!-- <p>Check out our latest collections</p> -->
                            <!-- </div> -->
                        </div>
                    </div>
                </header>
                
                <div class="row sofaSlider">
                    <!-- === product-item === -->
                    <?php foreach ($mostSellingProducts as $product){ ?>
                        <?php  $this->load->view('site/product_cart_view',array('product' => $product)) ?>
                    <?php } ?>

                </div> <!--/row-->

                <!-- <div>
                    <button class="leftArrow"> Prev </button>
                    <button class="rightArrow"> Next </button>
                </div> -->

                <div class="row">
                    <a href="<?=base_url()?>collection/new"><img id="inspiration" style="width: 104%;margin-left: -2%;" title="Inspiration" src="<?= base_url()?>assets/images/site/cover.jpg" alt="inspiration"></a>&nbsp;
                </div>

                <div class="row" style="font-size: 5.5vw; margin-right: -3%;margin-left:-3%; text-align: center; margin-top: 1.5%; margin-bottom: 1%; font-style: oblique; font-family: sofia;color: #c10000; background-color: white;">
                    Low Prices Always - Amazing Styles
                </div>
                <div class="row" style="border: bold 1px; "></div>

                <div class="row " style=" margin-left: -3.2%;margin-right: -3%;">
                    <div class="col-md-6 col-sm-6 col-xs-s col-lg-6" style="margin-top: 3%; border: 5px solid #ac3e27; padding-left: 3%;">
                        <div style="width: 98%; ">
                        <header class="row" style="margin-bottom: 3%;">
                            <div class="row">
                                <div class="col-md-offset-2 col-md-8 text-center">
                                    <h2 class="title lineUnderText">BEDROOM</h2>
                                </div>
                            </div>
                        </header>
                        <div class="row twoProductSilder" style="margin-right: 2%;">
                            <?php foreach ($leftSliderProducts as $product){ ?>
                                <?php  $this->load->view('site/product_cart_view',array('product' => $product,'isTwoProduct' => true)) ?>
                            <?php } ?>
                        </div>
                    </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-s col-lg-6" style="margin-top: 3%; border: 5px solid #da945a;  padding-right: 2%; " >
                        <div style="width: 98%; ">
                        <header class="row" style="margin-bottom: 3%;">
                            <div class="row">
                                <div class="col-md-offset-2 col-md-8 text-center">
                                    <h2 class="title lineUnderText">TABLE</h2>
                                </div>
                            </div>
                        </header>
                        <div class="row twoProductSilder" style="margin-left: 2%;">
                            <?php foreach ($rightSliderProducts as $product){ ?>
                                <?php  $this->load->view('site/product_cart_view',array('product' => $product,'isTwoProduct' => true)) ?>
                            <?php } ?>
                        </div>
                        </div>
                    </div>

                </div> <!--/row-->            
                <!-- ========================  Product info popup - quick view ======================== -->
            
                 <header style="margin-top: 4%;">
                    <div class="row">
                        <div class="col-md-offset-2 col-md-8 text-center">
                            <h2 class="title lineUnderText">CATEGORIES</h2>
                        </div>
                    </div>
                </header>
                
                <div class="row">
                    <?php foreach ($collections as $collection) { ?>
                        <?php  $this->load->view('site/category_cart_view',array('category' => $collection)) ?>
                    <?php } ?>

                </div> <!--/row-->

                
            </div> <!--/container-->
        </section>


<script type="text/javascript">
    $(document).ready(function(){

        $('.sofaSlider').slick({
          slidesToShow: 4,
          slidesToScroll: 1,
          // autoplay: true,
          arrows : true,
          // autoplaySpeed: 2000,
          // centerMode: true,
          responsive: [
            {
              breakpoint: 1024,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
                infinite: true,
                dots: true
              }
            },
            {
              breakpoint: 600,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 2
              }
            },
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1
              }
            }
          ]
        });

        $('.twoProductSilder').slick({
          slidesToShow: 2,
          slidesToScroll: 1,
          // autoplay: true,
          // centerMode : true,
          arrows : true,
          // autoplaySpeed: 2000,
          responsive: [
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1
              }
            }
          ]
        });

        // $('.leftArrow').on('click', function(){
        //     $('.x').slick("slickPrev");
        // });
        // $('.rightArrow').on('click', function(){
        //     $('.x').slick("slickNext");
        // });
    });
</script>

<style type="text/css">
    
</style>