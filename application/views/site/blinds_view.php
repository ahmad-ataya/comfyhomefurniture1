	<link rel="icon" href="image/favicon.png" type="image/png" sizes="16x16">
	<link id="style-css" rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/site//blind_style.css">


	<section class="main-header" style="height: 50ex;background-image:url(<?=base_url()?>assets/images/site/categories/blindsmain.png)">
        <header>
            <div class="container">
                <h1 class="h2 title">Blinds</h1>
                
            </div>
        </header>
    </section>


    <section class="products" style="padding-top: 40px;">

	    <div class="" style="margin-left: 2%;margin-right: 2%; padding-right: 1%;padding-left: 1%; ">
	        <div class="row">

	            <!-- === product-items === -->

	            <div class="col-md-12 col-xs-12">
	                <div class="row">
	                	<!-- Content Section -->
					<div class="section section-contents section-pad bdr-top">
						<div class="container">
							<div class=" row">
								<div class="wide-md text-center">
									<h2>OUR PRODUCTS</h2>
									<p class="lead">We offer over 300 different designs with super quality fabrics, free measurement and home-in consultation and installation, covered by a five year limited warranty.  </p>
								</div>
								<div class="clear"></div>
								<div class="gaps"></div>
								<!-- Gallery -->
								<div class="row" style="margin-bottom:20px;">
									<div class="col-md-2"></div>
									<div class="col-md-4 ">
									    <div class="col-md-3">
									    	<label style="line-height: 2.5; text-align: center;">Width</label>
									    </div>
									    <div class="col-md-9">
									    	<input type="text" class="form-control " placeholder="1" aria-label="First name" id="blindsWidth" value="1">
									    </div>
									</div>
								  	<div class="col-md-4 ">
									    <div class="col-md-3">
									    	<label style="line-height: 2.5; text-align: center;">Height</label>
									    </div>
									    <div class="col-md-9">
									    	<input type="text" class="form-control " placeholder="1" aria-label="First name" id="blindsHeight" value="1">
									    </div>
									</div>
 
								</div>
									
								<div class="gallery gallery-col3 gallery-grids gallery-with-caption hover-zoom">

									<ul class="gallery-list">
										<?php foreach ($blinds as $blind) { ?>	
											<li>
												<div class="gallery-item">
													<img src="<?=base_url()?>assets/images/site/blinds/<?=$blind->image;?>" alt="Name of Photo" style="width: 366px;height: 244px;">
													<div class="gallery-item-link">
														<span class="link-block">
															
															<a class="link link-popup image-lightbox" href="<?=base_url()?>assets/images/site/blinds/<?=$blind->image;?>" title="<?=$blind->title_en?>"><em class="fa fa-arrows-alt"></em></a>
														</span>
													</div>
													<div class="gallery-item-caption light">
														<p class="item-cat"><?=$blind->title_en; ?></p>
														<h4 class="item-title">
															<span class="h3 blindPrice" price = '<?=$blind->price;?>' discount1 = '<?=$blind->discount1;?>' discount2 = '<?=$blind->discount2;?>' discount3 = '<?=$blind->discount3;?>' discount4 = '<?=$blind->discount4;?>' style="color: #c10000;float: right;font-size: larger; margin-bottom:0;">
									                            <?php if($blind->discount1 > 0 ){ ?>
									                                <sub><small style=" font-size: medium;color: black; text-decoration: line-through;">$ <?= round($blind->price,2);?></small></sub>
									                            <?php } ?>
									                            $ <?= round($blind->price - (($blind->discount1*$blind->price/100)),2);?>
									                        </span>
									                    </h4>
													</div>
												</div>	
											</li>
										<?php } ?>
									</ul>

								</div>
								<!-- Gallery #End -->
							</div>
						</div>		
					</div>
	<!-- End Section -->
                  
                </div>
            </div>
        </div><!--/row-->
    </div><!--/container-->
</section>

<?php $this->load->view('site/footer_internal_pages'); ?>



	
	
<script type="text/javascript">
	var updatePrice = function(){
		var width = $('#blindsWidth').val();
		var height = $('#blindsHeight').val();
		$('.blindPrice').filter(function(index,item){
			var price = $(this).attr('price') ;
			var discount1 = $(this).attr('discount1');
			var discount2 = $(this).attr('discount2');
			var discount3 = $(this).attr('discount3');
			var discount4 = $(this).attr('discount4');
			newPriceHtml = '';
			var newDiscount = 0;
			var newPrice = price * height * width;
			var wXh = height * width;

			var discount = discount1;

			if(wXh > 1000 && wXh <= 1500) discount = discount2; 
			if(wXh > 1500 && wXh <= 2000) discount = discount2; 
			if(wXh > 2000 && wXh <= 2500) discount = discount3; 
			if(wXh > 2500) discount = discount4; 

			if(	newPrice > 0){
				if(discount > 0){
					newDiscount = (discount*newPrice/100);
					newPriceHtml += '<sub><small style="font-size: medium;color: black; text-decoration: line-through;">$ '+(Math.round(newPrice*100)/100)+'</small></sub>';
				}

				newPriceHtml += '$ '+(Math.round((newPrice - newDiscount)*100)/100);

				$(this).html(newPriceHtml)
			}
			else
				$(this).html('');

		})
	}

	$('#blindsWidth').change(updatePrice);
	$('#blindsHeight').change(updatePrice);
</script>




                

	<!-- JavaScript Bundle -->
	<script src="<?= base_url();?>assets/js/site/jquery.bundle.js"></script>
	<!-- Theme Script init() -->
	<script src="<?= base_url();?>assets/js/site/script.js"></script>
	<!-- End script -->
	<script>
	let mainNavLinks = document.querySelectorAll("nav ul li a");
let mainSections = document.querySelectorAll("main section");

let lastId;
let cur = [];



window.addEventListener("scroll", event => {
  let fromTop = window.scrollY;

  mainNavLinks.forEach(link => {
    let section = document.querySelector(link.hash);

    if (
      section.offsetTop <= fromTop &&
      section.offsetTop + section.offsetHeight > fromTop
    ) {
      link.classList.add("current");
    } else {
      link.classList.remove("current");
    }
  });
});
	</script>
	
	<script>
	$('a').click(function(){
    $('html, body').animate({
        scrollTop: $( $(this).attr('href') ).offset().top
    }, 800);
    return false;
});
	</script>
