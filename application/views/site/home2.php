 <!-- ========================  Header content ======================== -->

        <section class="header-content">

            <div class="owl-slider">
                <!-- === slide item === -->
                <?php  foreach ($sliders as $slider){ ?>
                    <div class="item" style="background-image:url(<?=base_url()?>assets/images/site/slider/<?=$slider->image?>)">
                        <div class="box">
                            <div class="container">
                                <h2 class="title animated h1" data-animation="fadeInDown"><?= $slider->{'title_'.$langCode}?></h2>
                                <div class="animated" data-animation="fadeInUp">
                                    <?= $slider->description;?>
                                </div>
                                <?php if($slider->link != ''){ ?>
                                    <div class="animated" data-animation="fadeInUp">
                                        <a href="<?=base_url().$slider->link?>" class="btn btn-clean">More Details</a>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                <?php }?>
            </div> <!--/owl-slider-->
        </section>

        <!-- ========================  Icons slider ======================== -->
<!--
        <section class="owl-icons-wrapper owl-icons-frontpage">


            <header class="hidden">
                <h2>Product categories</h2>
            </header>

            <div class="container">

                <div class="owl-icons">

                    <?php foreach ($categories as $category){ ?>
                        <a href="<?=base_url()?>category/<?=$category->route;?>">
                            <figure>
                                <i class="f-icon <?=($category->icon)?$category->icon:'f-icon-sofa'?>"></i>
                                <figcaption><?=$category->{'title_'.$langCode}?></figcaption>
                            </figure>
                        </a>
                    <?php } ?>

                </div> 
            </div> 
        </section>
-->
        <!-- ========================  Products widget ======================== -->

        <section class="products">

            <div class="container">

                <!-- === header title === -->

                <header>
                    <div class="row">
                        <div class="col-md-offset-2 col-md-8 text-center">
                            <h2 class="title">Popular products</h2>
                            <div class="text">
                                <p>Check out our latest collections</p>
                            </div>
                        </div>
                    </div>
                </header>
                
                <div class="row x">

                    <!-- === product-item === -->
                    <?php foreach ($popularProducts as $product){ ?>
                        <?php  $this->load->view('site/product_cart_view',array('product' => $product)) ?>
                    <?php } ?>

                    

                </div> <!--/row-->

            
                <!-- ========================  Product info popup - quick view ======================== -->

                <div class="popup-main mfp-hide" id="productid1">

                    <!-- === product popup === -->

                    <div class="product">

                        <!-- === popup-title === -->

                        <div class="popup-title">
                            <div class="h1 title">Laura <small>product category</small></div>
                        </div>

                        <!-- === product gallery === -->

                        <div class="owl-product-gallery">
                            <img src="<?=base_url()?>assets/images/site/product-1.png" alt="" width="640" />
                            <img src="<?=base_url()?>assets/images/site/product-2.png" alt="" width="640" />
                            <img src="<?=base_url()?>assets/images/site/product-3.png" alt="" width="640" />
                            <img src="<?=base_url()?>assets/images/site/product-4.png" alt="" width="640" />
                        </div>

                        <!-- === product-popup-info === -->

                        <div class="popup-content">
                            <div class="product-info-wrapper">
                                <div class="row">

                                    <!-- === left-column === -->

                                    <div class="col-sm-6">
                                        <div class="info-box">
                                            <strong>Maifacturer</strong>
                                            <span>Brand name</span>
                                        </div>
                                        <div class="info-box">
                                            <strong>Materials</strong>
                                            <span>Wood, Leather, Acrylic</span>
                                        </div>
                                        <div class="info-box">
                                            <strong>Availability</strong>
                                            <span><i class="fa fa-check-square-o"></i> in stock</span>
                                        </div>
                                    </div>

                                    <!-- === right-column === -->

                                    <div class="col-sm-6">
                                        <div class="info-box">
                                            <strong>Available Colors</strong>
                                            <div class="product-colors clearfix">
                                                <span class="color-btn color-btn-red"></span>
                                                <span class="color-btn color-btn-blue checked"></span>
                                                <span class="color-btn color-btn-green"></span>
                                                <span class="color-btn color-btn-gray"></span>
                                                <span class="color-btn color-btn-biege"></span>
                                            </div>
                                        </div>
                                        <div class="info-box">
                                            <strong>Choose size</strong>
                                            <div class="product-colors clearfix">
                                                <span class="color-btn color-btn-biege">S</span>
                                                <span class="color-btn color-btn-biege checked">M</span>
                                                <span class="color-btn color-btn-biege">XL</span>
                                                <span class="color-btn color-btn-biege">XXL</span>
                                            </div>
                                        </div>
                                    </div>

                                </div> <!--/row-->
                            </div> <!--/product-info-wrapper-->
                        </div> <!--/popup-content-->
                        <!-- === product-popup-footer === -->

                        <div class="popup-table">
                            <div class="popup-cell">
                                <div class="price">
                                    <span class="h3">$ 1999,00 <small>$ 2999,00</small></span>
                                </div>
                            </div>
                            <div class="popup-cell">
                                <div class="popup-buttons">
                                    <a href="product.html"><span class="icon icon-eye"></span> <span class="hidden-xs">View more</span></a>
                                    <!-- <a href="javascript:void(0);"><span class="icon icon-cart"></span> <span class="hidden-xs">Buy</span></a> -->
                                </div>
                            </div>
                        </div>

                    </div> <!--/product-->
                </div> <!--popup-main-->
            </div> <!--/container-->
        </section>