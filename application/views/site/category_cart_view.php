<div class="col-md-4 col-sm-4 col-xs-12" style="margin-bottom: 10px;">
    <article>
        <div class="info">
              <!--   <span class="add-favorite added">
                    <a href="javascript:void(0);" data-title="Add to favorites" data-title-added="Added to favorites list"><i class="icon icon-heart"></i></a>
                </span> -->
            <span>
                <a href="<?=base_url()?>collection/<?=($category->route != '')?$category->route:$category->id?>"  data-title="Quick wiew"><i class="icon icon-eye"></i></a>
            </span>
        </div>
      
        <div class="figure-grid">
            <div class="image">
                <a href="<?=base_url()?>collection/<?=($category->route != '')?$category->route:$category->id?>" >
                    <img src="<?=base_url()?>assets/images/site/collections/<?=$category->image?>" alt="" width="360" height="<?=($isTwoProduct)?250:250?>" />
                </a>
            </div>
            <div class="text row" style="margin-left:0px;margin-right: 0px; margin-top: 10px;">   
                <div class="col-sm-12 col-xs-12">
                    <h2 class="title h4" style="text-align: center;"><a href="product.html"><?=$category->{'title_'.$langCode}?></a></h2>
                </div>
            </div>

            <div class="text row" style="margin-left:0px;margin-right: 0px; margin-top: -12px; padding-top: 0px">
                <div class="col-sm-12 col-xs-12"  style="text-align: center;">
                        <span style="color: block;font-size: 14px;">From <span style="font-size: 24px; color: red; font-family: \"Montserrat\", sans-serif;"> <?=$category->price;?>$<span></span>
                </div>
            </div>

        </div>
    </article>
</div>