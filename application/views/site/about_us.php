<section class="banner" style="background-image:url(<?=base_url()?>assets/images/site/gallery-4.jpg)" id="aboutUs">
    <div class="container">
        <div class="row">
            <div class="col-md-offset-2 col-md-8 text-center" style="width: 78%; margin-left: 12%; line-height: 1.8;">
                <h2 class="title">About US</h2>
                <br/>
                <p>
                    Comfy home furniture is a local Edmonton family business, founded in 2019 with a vision to provide best customer experience, Searching for high quality and suitable prices for the latest furniture designs around the world.
                </p>
                <p> We believe in building a client community via trust is the best way to build our business. </p>
                <p> We are looking for the best furniture around the world. </p>
                <p>
                    We import our stock from major suppliers and manufacturers across the world, which means that our customers have the option of custom made furniture on many styles, find prices and patterns that suit your needs, your job and your style. 
                </p>
                <p>
                    We offer fashionable, reliable and elegant products to our customers frequently throughout the year so we are always on trend.
                     We run our business smartly, keeping our overheads low, so we can pass the savings over to our customers.
                </p>
                <p style="font-size: 15.5px;"> 
                    So, check out our comfy home furniture ltd. for a furniture buying experience which will put you first and won't break the bank.
                </p>
            </div>
        </div>
    </div>
</section>