<?php if(!$category->isNewCollection ){ ?>
    <section class="main-header" style="height: 50ex;background-image:url(<?=base_url()?>assets/images/site/<?=isset($isCollection)?'collections/':'categories/'?><?=$category->image?>)">
        <header>
            <div class="container">
               <!--  <h1 class="h2 title"><?=$category->{'title_'.$langCode}?></h1>
                <ol class="breadcrumb breadcrumb-inverted">
                    <li><a href="<?=base_url()?>"><span class="icon icon-home"></span></a></li>
                    <?php if($category->parent_id != 0 && isset($categoriesInfo[$category->parent_id])){ ?>
                        <li><a href="<?=base_url()?>category/<?=$categoriesInfo[$category->parent_id]->route?>"><?=$categoriesInfo[$category->parent_id]->{'title_'.$langCode}?></a></li>
                    <?php } ?>
                    <li><a class="active"  href="<?=base_url()?>category/<?=$category->route?>"><?=$category->{'title_'.$langCode}?></a></li>
                </ol> -->
            </div>
        </header>
    </section>
<?php } ?>
<!-- ========================  Icons slider ======================== -->

<section class="products" style="padding-top: 40px;">

    <div class="" style="margin-left: 2%;margin-right: 2%; padding-right: 1%;padding-left: 1%; ">

        <header>
            <div class="row">
                <div class="col-md-offset-2 col-md-8 text-center" style="text-align: left; margin-left: 2px;">
                    <h2 class="title"><?=$category->{'title_'.$langCode}?></h2>
                    <ol class="breadcrumb breadcrumb-inverted" >
                        <li><a href="<?=base_url()?>" style="color: black;"><span class="icon icon-home"></span></a></li>
                        <?php if($category->parent_id != 0 && isset($categoriesInfo[$category->parent_id])){ ?>
                            <li><a style="color: black;" href="<?=base_url()?>category/<?=$categoriesInfo[$category->parent_id]->route?>"><?=$categoriesInfo[$category->parent_id]->{'title_'.$langCode}?></a></li>
                        <?php } ?>
                        <li><a class="active"  href="<?=base_url()?>category/<?=$category->route?>"><?=$category->{'title_'.$langCode}?></a></li>
                    </ol>
                </div>
            </div>
        </header>

        <div class="row">

            <!-- === product-items === -->

            <div class="col-md-12 col-xs-12">
                <div class="row">

                    <!-- === product-item === -->
                    <?php foreach ($products as $product){ ?>
                        <?php $this->load->view('site/product_cart_view',array('product' => $product)); ?>
                    <?php } ?>
                </div>
            </div>
        </div><!--/row-->
    </div><!--/container-->
</section>


<?php $this->load->view('site/footer_internal_pages'); ?>