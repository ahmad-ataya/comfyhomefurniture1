<section class="products" style="padding-top: 0px;">

    <div class="" style="margin-left: 2%;margin-right: 2%; padding-right: 1%;padding-left: 1%; ">
        <div class="row" style="font-size: 5.5vw; margin-right: -3%;margin-left:-3%; text-align: center; margin-top: 1.5%; margin-bottom: 1%; font-style: oblique; font-family: sofia;color: #c10000; background-color: white;">
                    Low Prices Always - Amazing Styles
        </div>
        <div class="row" style="border: bold 1px; "></div>
        
        <header style="margin-top: 4%;">
            <div class="row">
                <div class="col-md-offset-2 col-md-8 text-center">
                    <h2 class="title lineUnderText">CATEGORIES</h2>
                </div>
            </div>
        </header>
        
        <div class="row">
            <?php foreach ($collections as $collection) { ?>
                <?php  $this->load->view('site/category_cart_view',array('category' => $collection)) ?>
            <?php } ?>

        </div> <!--/row-->

    </div>
</section>