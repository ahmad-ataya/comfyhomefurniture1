﻿<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- Mobile Web-app fullscreen -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">

    <!-- Meta tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="og:image" property="og:image" content="<?=base_url()?>assets/images/site/fbimage.png">
    <link rel="icon" href="favicon.ico">

    <!--Title-->
    <title>Comfy Home Furniture</title>

    <!--CSS styles-->
    <link rel="stylesheet" media="all" href="<?= base_url()?>assets/css/bootstrap.min.css" />
    <link rel="stylesheet" media="all" href="<?= base_url()?>assets/css/font-awesome.min.css" />
    <link rel="stylesheet" media="all" href="<?= base_url()?>assets/css/site/animate.css" />
    <link rel="stylesheet" media="all" href="<?= base_url()?>assets/css/site/furniture-icons.css" />
    <link rel="stylesheet" media="all" href="<?= base_url()?>assets/css/site/linear-icons.css" />
    <link rel="stylesheet" media="all" href="<?= base_url()?>assets/css/site/magnific-popup.css" />
    <link rel="stylesheet" media="all" href="<?= base_url()?>assets/css/site/owl.carousel.css" />
    <link rel="stylesheet" media="all" href="<?= base_url()?>assets/css/site/ion-range-slider.css" />
    <link rel="stylesheet" media="all" href="<?= base_url()?>assets/css/site/theme.css" />
    <link rel="stylesheet" type="text/css" href="<?= base_url()?>assets/css/site/slick.css"/>
    <link rel="stylesheet" type="text/css" href="<?= base_url()?>assets/css/site/slick-theme.css"/>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Sofia">
    <link href="https://fonts.googleapis.com/css2?family=Raleway:ital@1&display=swap" rel="stylesheet">
    <!--Google fonts-->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600&amp;subset=latin-ext" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="<?= base_url()?>assets/js/jquery.min.js"></script>
    <script src="<?= base_url()?>assets/js/jquery.bootstrap.min.js"></script>
    <style>
body {margin:0;height:2000px; font-family: 'Raleway', sans-serif;}

.icon-bar {
  position: fixed;
  top: 50%;
  -webkit-transform: translateY(-50%);
  -ms-transform: translateY(-50%);
  transform: translateY(-50%);
  z-index: 100;
}

.icon-bar a {
  display: block;
  text-align: center;
  padding: 16px;
  transition: all 0.3s ease;
  color: white;
  font-size: 20px;
}

.icon-bar a:hover {
  background-color: #000;
}

.facebook {
  background: #3B5998;
  color: white;
}

.instagram {
  background-image: url('<?=base_url();?>assets/images/site/insta.jpg');
  background-size: cover;
  color: white;
}



.google {
  background: #dd4b39;
  color: white;
}

.linkedin {
  background: #007bb5;
  color: white;
}

.youtube {
  background: #bb0000;
  color: white;
}

.content {
  margin-left: 75px;
  font-size: 30px;
}

.fa {
  padding: 8px;
  font-size: 25px;
  /*width: 30px;*/
  text-align: center;
  text-decoration: none;
  margin: 5px 2px;
  border-radius: 50%;
}
.fa:hover {
    opacity: 0.7;
}

.fa-facebook {
  /*background: #c10000;*/
  /*color: white;*/
  color: #c10000;background-color: white;
}.fa-instagram {
  /*background: #c10000;*/
  /*color: white;*/
  color: #c10000;background-color: white;
}
</style>


<!-- Start of  Zendesk Widget script -->
<!-- <script id="ze-snippet" src="https://static.zdassets.com/ekr/snippet.js?key=75cac4bb-f796-4741-96b6-5c2a943d3e1c"> </script> -->
<!-- End of  Zendesk Widget script -->

</head>

<body>
    <!-- <div class="icon-bar">
  <a href="https://www.facebook.com/COMFY-home-furniture-111819997176163/" target="_blank" class="facebook"><i class="fa fa-facebook"></i></a> 
  <a href="https://www.instagram.com/comfy.home.furniture/" target="_blank" class="instagram"><i class="fa fa-instagram"></i></a> 
</div>
 -->
    <div class="page-loader"></div>

    <div class="container aboveNavBar" style=" color: black; margin-top: 3%; margin-bottom: 2%">
        <div class="row">
            <div class="col-md-3" style="text-align: left;">
                <a href="javascript:void(0)" id="contactsBtn" class="btn btn-success"  style="background-color: white; color: #c10000;border-color: #c10000; border-radius: 20%; border-style: none;font-size: 20px;">Contact US</a>
                
            </div>
            <a href="<?=base_url()?>" class="col-md-6">
               <img src="<?=base_url();?>assets/images/site/logo2.jpg" width="260" style="display: block; margin: auto;">
            </a>
            <div class="col-md-3" style="text-align: right;">
                
                <a href="https://www.facebook.com/COMFY-home-furniture-111819997176163/" target="_blank" class="fa fa-facebook" style=""></a>
                <a href="https://www.instagram.com/comfy.home.furniture/" target="_blank" class="fa fa-instagram" style=""></a>
            </div>
        </div>
    </div>
    <div class="wrapper">
        <!-- ======================== Navigation ======================== -->


        <nav class="navbar-fixed" style="background-color: white;color: #c10000;">

            <div class="" style="margin-right: 3%; margin-left: 3%;">

                <!-- ==========  Top navigation ========== -->

                <!-- <div class="navigation navigation-top clearfix"> -->
                    <!-- <ul> -->
                        <!--add active class for current page-->

                        <!-- <li><a href="#"><i class="fa fa-facebook"></i></a></li> -->
                        <!-- <li><a href="#"><i class="fa fa-twitter"></i></a></li> -->

                        <!--Language selector-->

                       <!--  <li class="nav-settings">
                            <a href="javascript:void(0);" class="nav-settings-value"> ENG</a>
                            <ul class="nav-settings-list">
                                <li>ENG</li>
                                <li>FRA</li>
                            </ul>
                        </li> -->
                        <!-- <li><a href="javascript:void(0);" class="open-login"><i class="icon icon-user"></i></a></li> -->
                        <!-- <li><a href="javascript:void(0);" class="open-search"><i class="icon icon-magnifier"></i></a></li> -->
                        <!-- <li><a href="javascript:void(0);" class="open-cart"><i class="icon icon-cart"></i> <span>3</span></a></li> -->
                    <!-- </ul> -->
                <!-- </div>  -->

                <!-- ==========  Main navigation ========== -->

                <div class="navigation navigation-main">

                    <!-- Setup your logo here-->

                    <a href="<?=base_url();?>" class="logo nav2"><img src="<?=base_url()?>assets/images/site/logo.png" alt="" style= "margin-top:10px;"/></a>

                    <!-- Mobile toggle menu -->

                    <a href="#" class="open-menu"><i class="icon icon-menu"></i></a>

                    <!-- Convertible menu (mobile/desktop)-->

                    <div class="floating-menu">

                        <!-- Mobile toggle menu trigger-->

                        <div class="close-menu-wrapper">
                            <span class="close-menu"><i class="icon icon-cross"></i></span>
                        </div>

                        <ul>
                            <li><a href="<?=base_url()?>">Home</a></li> 
                            <li><a href="<?=base_url();?>about-us" >About Us</a></li>

                            <?php foreach ($categories as $category){ ?>
                                <?php if(!isset($allCategories[$category->id])){ ?>
                                    <?php if($category->route == 'BLINDS'){ ?>
                                        <li><a href="<?=base_url()?>blinds"><?=$category->{'title_'.$langCode}?></a></li>
                                    <?php }else{ ?>
                                        <li><a href="<?=base_url()?>category/<?=$category->route;?>"><?=$category->{'title_'.$langCode}?></a></li>
                                    <?php } ?>
                                <?php }else{ ?> 
                                    <li>
                                        <a href="<?=base_url()?>category/<?=$category->route;?>"><?=$category->{'title_'.$langCode}?><span class="open-dropdown"><i class="fa fa-angle-down"></i></span></a>
                                        <div class="navbar-dropdown navbar-dropdown-single">
                                            <div class="navbar-box">
                                                <div class="box-2">
                                                    <div class="box clearfix">
                                                        <ul>
                                                            <?php foreach($allCategories[$category->id] as $subCategory){ ?>
                                                                <li><a href="<?=base_url()?>category/<?=$subCategory->route;?>"><?=$subCategory->{'title_'.$langCode}?></a></li>
                                                            <?php } ?>
                                                        </ul> <!--/row-->
                                                    </div> <!--/categories-->
                                                </div> <!--/box-2-->
                                            </div> <!--/navbar-box-->
                                        </div> <!--/navbar-dropdown-->
                                    </li>
                                <?php } ?>
                            <?php } ?>

                            <!-- <li><a href="<?=base_url()?>blinds">Blinds</a></li> -->
                            <!-- <li><a href="javascript:void(0)" id="contactsBtn">Contact US</a></li> -->
                             <a href="https://www.instagram.com/comfy.home.furniture/" target="_blank" class="fa fa-instagram nav2" style=" float: right; margin-top: 2%; display:none;"></a>
                             <a href="https://www.facebook.com/COMFY-home-furniture-111819997176163/" target="_blank" class="fa fa-facebook nav2" style="float: right; margin-top: 2%; display: none; "></a>
                        </ul>
                    </div> <!--/floating-menu-->
                </div> <!--/navigation-main-->
            </div> <!--/container-->
        </nav>


        <?= $content; ?> 



         <!-- ========================  Banner ======================== -->

        <!-- <section class="banner" style="background-image:url(<?=base_url()?>assets/images/site/gallery-4.jpg)" id="aboutUs">
            <div class="container">
                <div class="row">
                    <div class="col-md-offset-2 col-md-8 text-center" style="width: 78%; margin-left: 12%; line-height: 1.8;">
                        <h2 class="title">About US</h2>
                        <br/>
                        <p>
                            Comfy home furniture is a local Edmonton family business, founded in 2019 with a vision to provide best customer experience, Searching for high quality and suitable prices for the latest furniture designs around the world.
                        </p>
                        <p> We believe in building a client community via trust is the best way to build our business. </p>
                        <p> We are looking for the best furniture around the world. </p>
                        <p>
                            We import our stock from major suppliers and manufacturers across the world, which means that our customers have the option of custom made furniture on many styles, find prices and patterns that suit your needs, your job and your style. 
                        </p>
                        <p>
                            We offer fashionable, reliable and elegant products to our customers frequently throughout the year so we are always on trend.
                             We run our business smartly, keeping our overheads low, so we can pass the savings over to our customers.
                        </p>
                        <p style="font-size: 15.5px;"> 
                            So, check out our comfy home furniture ltd. for a furniture buying experience which will put you first and won't break the bank.
                        </p>
                    </div>
                </div>
            </div>
        </section> -->
            
        <!-- ========================  Contact ======================== -->

        <section id="page-contact" class="contact contact-single">

            <!-- === Goolge map === -->

            <div id="map">
                
            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d2369.5311131107246!2d-113.5953457!3d53.5661372!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x53a02146683f8c9f%3A0x54c81e3c3533ed52!2s15791%20116%20Ave%20NW%2C%20Edmonton%2C%20AB%20T5M%203W1%2C%20Canada!5e0!3m2!1sfr!2sbh!4v1589080409586!5m2!1sfr!2sbh" width="100%" height="100%" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
            </div>
            <div class="container">

                <div class="row">

                    <div class="col-sm-10 col-sm-offset-1">

                        <div class="contact-block">

                            <div class="contact-info">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <figure class="text-center">
                                            <span class="icon icon-map-marker"></span>
                                            <figcaption>
                                                <strong>Where are we?</strong>
                                                <span>15791 116 Ave NW, Edmonton,<br />AB T5M 3W1, Canada</span>
                                            </figcaption>
                                        </figure>
                                    </div>
                                    <div class="col-sm-4">
                                        <figure class="text-center">
                                            <span class="icon icon-phone"></span>
                                            <figcaption>
                                                <strong>Call us</strong>
                                                <span>
                                                    <strong>T</strong> +1 780 233 7841 <br />
                                                </span>
                                            </figcaption>
                                        </figure>
                                    </div>
                                    <div class="col-sm-4">
                                        <figure class="text-center">
                                            <span class="icon icon-clock"></span>
                                            <figcaption>
                                                <strong>Working hours</strong>
                                                <span>
                                                    <strong>Wednesday</strong> - Saturday: 11 am - 6 pm <br />
                                                    <strong>Sunday</strong> - Monday: 12 am - 6 pm <br />
                                                    <strong>Tuesday</strong> Closed
                                                </span>
                                            </figcaption>
                                        </figure>
                                    </div>
                                </div>
                            </div>

                            <div class="banner">
                                <div class="row">
                                    <div class="col-md-offset-1 col-md-10 text-center">
                                        <h2 class="title">Send an email</h2>
                                        <p>
                                            Thanks for your interest to our store. We believe in creativity as one of the major forces of progress.
                                            Please use this form if you have any questions about our products and we'll get back with you very soon.
                                        </p>

                                        <div class="contact-form-wrapper">

                                            <a class="btn btn-clean open-form" data-text-open="Contact us via form" data-text-close="Close form" style="color: white!important;">Contact us via form</a>

                                            <div class="contact-form clearfix">
                                                <form action="<?=base_url()?>home/contactUS" method="post">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <input type="text" value="" name="userName" class="form-control" placeholder="Your name" required="required">
                                                            </div>
                                                        </div>

                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <input type="email" value="" name="userEmail" class="form-control" placeholder="Your email" required="required">
                                                            </div>
                                                        </div>

                                                        <div class="col-md-12">

                                                            <div class="form-group">
                                                                <input type="text" value="" name="userSubject" class="form-control" placeholder="Subject" required="required">
                                                            </div>
                                                        </div>

                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <textarea name="userMessage" class="form-control" placeholder="Your message" rows="10"></textarea>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-12 text-center">
                                                            <input type="submit" class="btn btn-clean" value="Send message" />
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>


                    </div><!--col-sm-8-->
                </div> <!--/row-->
            </div><!--/container-->
        </section>

      

    
        <!-- ================== Footer  ================== -->

        <footer style="background-color: white;  ">
            <div class="container">

                <!--footer showroom-->
                <div class="footer-showroom" style="margin-bottom: 0px;">
                    <div class="row">
                        <div class="col-sm-8">
                            <h2>Comfy Home Furniture</h2>
                            <p>15791 116 Ave NW, Edmonton, AB T5M 3W1, Canada</p>
                            <p>Wednesday - Saturday: 11 am - 6 pm &nbsp; &nbsp;  | &nbsp; &nbsp; Sunday - Monday : 12 am - 6 pm &nbsp; &nbsp; | &nbsp; &nbsp; Tuesday: Closed</p>
                        </div>
                        <div class="col-sm-4 text-center">
                            <div class="call-us h4"><span class="icon icon-phone-handset"></span> +1 780 233 7841</div>
                            <a href="javascript:void(0);" id="getDirections" class="btn btn-clean"><span class="icon icon-map-marker"></span> Get directions</a>
                        </div>
                    </div>
                </div>

                <!--footer social-->

                <div class="footer-social">
                    <div class="row">
                        <div class="col-sm-6">
                            &nbsp; <a href="#">Privacy policy</a>
                        </div>
                        <div class="col-sm-6 links">
                            <ul>
                                <li><a href="https://www.facebook.com/COMFY-home-furniture-111819997176163/" target="_blank"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="https://www.instagram.com/comfy.home.furniture/" target="_blank"><i class="fa fa-instagram"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </footer>

    </div> <!--/wrapper-->

    <!--JS files-->
    <script src="<?= base_url()?>assets/js/site/jquery.magnific-popup.js"></script>
    <script src="<?= base_url()?>assets/js/site/jquery.owl.carousel.js"></script>
    <script src="<?= base_url()?>assets/js/site/jquery.ion.rangeSlider.js"></script>
    <script src="<?= base_url()?>assets/js/site/jquery.isotope.pkgd.js"></script>
    <script src="<?= base_url()?>assets/js/site/main.js"></script>
    <script src="<?= base_url()?>assets/js/site/slick.js"></script>

    <script>
        function initMap() {
            var contentString =
            '<div class="map-info-window">' +
            '<p><img src="<?=base_url()?>assets/images/site/logo-dark.png" alt=""></p>' +
            '<p><strong>Mobel - Furniture factory</strong></p>' +
            '<p><i class="fa fa-map-marker"></i> 200 12th Ave, New York, NY 10001, USA</p>' +
            '<p><i class="fa fa-phone"></i> +12 33 444 555</p>' +
            '<p><i class="fa fa-clock-o"></i> 10am - 6pm</p>' +
            '</div>';

            var infowindow = new google.maps.InfoWindow({
                content: contentString
            });
            //set default pposition
            var myLatLng = { lat: 40.7459772, lng: -74.0545504 };
            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 12,
                center: myLatLng,
                styles: [{ "featureType": "administrative", "elementType": "all", "stylers": [{ "visibility": "on" }, { "saturation": -100 }, { "lightness": 20 }] }, { "featureType": "road", "elementType": "all", "stylers": [{ "visibility": "on" }, { "saturation": -100 }, { "lightness": 40 }] }, { "featureType": "water", "elementType": "all", "stylers": [{ "visibility": "on" }, { "saturation": -10 }, { "lightness": 30 }] }, { "featureType": "landscape.man_made", "elementType": "all", "stylers": [{ "visibility": "simplified" }, { "saturation": -60 }, { "lightness": 10 }] }, { "featureType": "landscape.natural", "elementType": "all", "stylers": [{ "visibility": "simplified" }, { "saturation": -60 }, { "lightness": 60 }] }, { "featureType": "poi", "elementType": "all", "stylers": [{ "visibility": "off" }, { "saturation": -100 }, { "lightness": 60 }] }, { "featureType": "transit", "elementType": "all", "stylers": [{ "visibility": "off" }, { "saturation": -100 }, { "lightness": 60 }] }]
            });
            //set marker
            var image = '<?=base_url()?>assets/images/site/map-icon.png';
            var marker = new google.maps.Marker({
                position: myLatLng,
                map: map,
                title: "Hello World!",
                icon: image
            });
            marker.addListener('click', function () {
                infowindow.open(map, marker);
            });
        }
    </script>
    <!-- <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCWGeWCGT5DmEIqEx9hk8c4xTVQqdCO2sY"></script> -->
  
</body>

</html>