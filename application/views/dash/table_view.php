<section class="content">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <!-- <div class="card-header"> -->
          <!-- <h3 class="card-title">DataTable with default features</h3> -->
        <!-- </div> -->
        <!-- /.card-header -->
        <div class="card-body">
          <?php if(isset($addPath)){ ?> 
            <a  style="position: absolute; z-index: 1" class="btn btn-success" href="<?=base_url()?><?=$addPath?>">Add New</a>
          <?php } ?> 
          <table id="example2" class="table table-bordered table-striped">
            <thead>
            <tr>
              <?php foreach ($tableKeys as $key => $value) { ?>
                <th><?=$value;?></th>
              <?php } ?>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($tableData as $row) { ?>
              <tr>
                <?php foreach ($tableKeys as $key => $value) { ?>
                  <?php if($key == 'actions') continue; ?>

                  <?php if(isset($keySpecific) && isset($keySpecific[$key])){ ?>
                    <td><?= str_replace('{value}',(isset($row->{$key}))?$row->{$key}:'',$keySpecific[$key]); ?></td>
                  <?php }else if($key == 'active'){ ?>
                    <td><?= (isset($row->{$key}) && $row->{$key} == 1)?'<span class="badge badge-success">Active</span>':'<span class="badge badge-danger">Not Active</span>'?></td>
                  <?php }else{ ?>
                    <td><?= (isset($row->{$key}))?$row->{$key}:''?></td>
                  <?php } ?>
                <?php } ?>
                <?php if(isset($tableKeys['actions']) && isset($actionsValue)){ ?>

                  <td><?= str_replace('{ID}',$row->id,$actionsValue);?></td>
                <?php } ?>
              </tr>
            <?php } ?>

            </tfoot>
          </table>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>



<script type="text/javascript">
  $(function () {
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true,
    });
  });
</script>