<section class="content">
  <div class="container-fluid">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- jquery validation -->
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title"><?=$activePage?></small></h3>
          </div>
          <!-- /.card-header -->
          <!-- form start -->
          <form class="form-horizontal" method="POST" action="<?=base_url()?>dashboard/categories/<?=isset($category)?'edit_save/'.$category->id:'add'?>" enctype="multipart/form-data">
            <div class="card-body">
              
              <div class="form-group row">
                <label for="inputEmail3" class="col-sm-2 col-form-label">Parent Category</label>
                <div class="col-sm-10">
                  <select class="form-control select2 " name="parent_id" style="height: 20px">
                      <option value="0">Choose One...</option>
                      <?php foreach ($categories as $cat) { if($cat->parent_id != 0) continue; ?>
                        <option value="<?=$cat->id?>" <?= (isset($category) && $category->parent_id == $cat->id)?'selected':''?> ><?=$cat->title_en; ?></option>
                      <?php } ?>
                  </select>
                </div>
              </div>

              <div class="form-group row">
                <label for="inputEmail3" class="col-sm-2 col-form-label">EN Title</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" placeholder="EN Title" name="title_en" value="<?=isset($category)?$category->title_en:set_value('title_en')?>">
                </div>
              </div>

              <div class="form-group row">
                <label for="inputEmail3" class="col-sm-2 col-form-label">FR Title</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" placeholder="FR Title" name="title_fr" value="<?=isset($category)?$category->title_fr:set_value('title_fr')?>">
                </div>
              </div>

               <div class="form-group row">
                <label for="inputEmail3" class="col-sm-2 col-form-label">Route</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" placeholder="Route" name="route" value="<?=isset($category)?$category->route:set_value('route')?>">
                </div>
              </div>

              <div class="form-group row">
                <label for="inputPassword3" class="col-sm-2 col-form-label">Active</label>
                <div class="col-sm-10">
                <input type="checkbox" name="active" <?=(isset($category) && $category->active == 1)?'checked':''?> data-bootstrap-switch data-off-color="danger" data-on-color="success">
                 
                </div>
              </div>


              <div class="form-group row">
                <label for="inputFirst Name3" class="col-sm-2 col-form-label">Image</label>
                <div class="col-sm-10">
                  <input type="file" id="fileInput" class="form-control"  placeholder="Image" name="file" value="">
                  <img id="previewImage" src="<?=isset($category->image)?base_url().'assets/images/site/categories/'.$category->image:'display: none;'?>" style="<?=isset($category->image)?'':'display: none;'?> width: 200px; margin-top: 20px;">
                </div>
              </div>

            </div>
            <!-- /.card-body -->
            <div class="card-footer">
              <button type="submit" class="btn btn-info">Submit</button>
            </div>
            <!-- /.card-footer -->
          </form>
        </div>
        <!-- /.card -->
        </div>
      <!--/.col (left) -->
      <!-- right column -->
      <div class="col-md-6">

      </div>
      <!--/.col (right) -->
    </div>
    <!-- /.row -->
  </div><!-- /.container-fluid -->
</section>


<script type="text/javascript">
  function readURL(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      
      reader.onload = function(e) {
        $('#previewImage').attr('src', e.target.result);
        $('#previewImage').show();
      }
      
      reader.readAsDataURL(input.files[0]);
    }
}

$("#fileInput").change(function() {
  readURL(this);
});
</script>