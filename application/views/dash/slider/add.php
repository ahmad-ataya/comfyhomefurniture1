<section class="content">
  <div class="container-fluid">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- jquery validation -->
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title"><?=$activePage?></small></h3>
          </div>
          <!-- /.card-header -->
          <!-- form start -->
          <form class="form-horizontal" method="POST" action="<?=base_url()?>dashboard/slider/<?=isset($slider)?'edit/'.$slider->id:'add'?>" enctype="multipart/form-data">
            <div class="card-body">
              
              <div class="form-group row">
                <label for="inputEmail3" class="col-sm-2 col-form-label">Link</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" placeholder="Link" name="link" value="<?=isset($slider)?$slider->link:set_value('link')?>">
                </div>
              </div>

              <div class="form-group row">
                <label for="inputPassword3" class="col-sm-2 col-form-label">Description</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" name="description" placeholder="Description" value="<?=isset($slider)?$slider->description:set_value('description')?>">
                </div>
              </div>


              <div class="form-group row">
                <label for="inputFirst Name3" class="col-sm-2 col-form-label">Image</label>
                <div class="col-sm-10">
                  <input type="file" id="fileInput" class="form-control"  placeholder="Image" name="file" value="">
                  <img id="previewImage" src="<?=isset($slider->image)?base_url().'assets/images/site/slider/'.$slider->image:'display: none;'?>" style="<?=isset($slider->image)?'':'display: none;'?> width: 200px; margin-top: 20px;">
                </div>
              </div>

            </div>
            <!-- /.card-body -->
            <div class="card-footer">
              <button type="submit" class="btn btn-info">Submit</button>
            </div>
            <!-- /.card-footer -->
          </form>
        </div>
        <!-- /.card -->
        </div>
      <!--/.col (left) -->
      <!-- right column -->
      <div class="col-md-6">

      </div>
      <!--/.col (right) -->
    </div>
    <!-- /.row -->
  </div><!-- /.container-fluid -->
</section>


<script type="text/javascript">
  function readURL(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      
      reader.onload = function(e) {
        $('#previewImage').attr('src', e.target.result);
        $('#previewImage').show();
      }
      
      reader.readAsDataURL(input.files[0]);
    }
}

$("#fileInput").change(function() {
  readURL(this);
});
</script>