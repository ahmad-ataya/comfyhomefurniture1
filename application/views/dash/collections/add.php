<section class="content">
  <div class="container-fluid">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- jquery validation -->
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title"><?=$activePage?></small></h3>
          </div>
          <!-- /.card-header -->
          <!-- form start -->
          <form class="form-horizontal" method="POST" action="<?=base_url()?>dashboard/collections/<?=isset($collection)?'edit_save/'.$collection->id:'add'?>" enctype="multipart/form-data">
            <div class="card-body">
              
              <div class="form-group row">
                <label for="inputEmail3" class="col-sm-2 col-form-label">Categories</label>
                <div class="col-sm-10">
                  <select class="form-control select2 " multiple="multiple" name="categories[]" style="height: 20px">
                      <?php foreach ($categories as $cat) {  ?>
                        <option value="<?=$cat->id?>" <?= (isset($collectionCategories) && in_array($cat->id, $collectionCategories))?'selected':''?> ><?=$cat->title_en; ?></option>
                      <?php } ?>
                  </select>
                </div>
              </div>

              <div class="form-group row">
                <label for="inputEmail3" class="col-sm-2 col-form-label">From Price</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" placeholder="From Price" name="price" value="<?=isset($collection)?$collection->price:set_value('price')?>">
                </div>
              </div>

              <div class="form-group row">
                <label for="inputEmail3" class="col-sm-2 col-form-label">EN Title</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" placeholder="EN Title" name="title_en" value="<?=isset($collection)?$collection->title_en:set_value('title_en')?>">
                </div>
              </div>

              <div class="form-group row">
                <label for="inputEmail3" class="col-sm-2 col-form-label">FR Title</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" placeholder="FR Title" name="title_fr" value="<?=isset($collection)?$collection->title_fr:set_value('title_fr')?>">
                </div>
              </div>

               <div class="form-group row">
                <label for="inputEmail3" class="col-sm-2 col-form-label">Route</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" placeholder="Route" name="route" value="<?=isset($collection)?$collection->route:set_value('route')?>">
                </div>
              </div>

              <div class="form-group row">
                <label for="inputPassword3" class="col-sm-2 col-form-label">Active</label>
                <div class="col-sm-10">
                <input type="checkbox" name="active" <?=(isset($collection) && $collection->active == 1)?'checked':''?> data-bootstrap-switch data-off-color="danger" data-on-color="success">
                 
                </div>
              </div>


              <div class="form-group row">
                <label for="inputFirst Name3" class="col-sm-2 col-form-label">Image</label>
                <div class="col-sm-10">
                  <input type="file" id="fileInput" class="form-control"  placeholder="Image" name="file" value="">
                  <img id="previewImage" src="<?=isset($collection->image)?base_url().'assets/images/site/collections/'.$collection->image:'display: none;'?>" style="<?=isset($collection->image)?'':'display: none;'?> width: 200px; margin-top: 20px;">
                </div>
              </div>

            </div>
            <!-- /.card-body -->
            <div class="card-footer">
              <button type="submit" class="btn btn-info">Submit</button>
            </div>
            <!-- /.card-footer -->
          </form>
        </div>
        <!-- /.card -->
        </div>
      <!--/.col (left) -->
      <!-- right column -->
      <div class="col-md-6">

      </div>
      <!--/.col (right) -->
    </div>
    <!-- /.row -->
  </div><!-- /.container-fluid -->
</section>


<script type="text/javascript">
  function readURL(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      
      reader.onload = function(e) {
        $('#previewImage').attr('src', e.target.result);
        $('#previewImage').show();
      }
      
      reader.readAsDataURL(input.files[0]);
    }
}

$("#fileInput").change(function() {
  readURL(this);
});
</script>