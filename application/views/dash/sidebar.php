<!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="<?= base_url()?>dashboard/home" class="brand-link">
      <img src="<?=base_url()?>assets/images/dash/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">AdminLTE 3</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="<?=base_url()?>assets/images/dash/mohammad.png" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block"><?= $user->first_name.' '.$user->last_name;?></a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
         

          <li class="nav-item has-treeview <?php if(isset($mainPage) && $mainPage == 'slider') echo 'menu-open'; ?>">
            <a href="#" class="nav-link <?php if(isset($mainPage) && $mainPage == 'slider') echo 'active'; ?>">
              <i class="nav-icon far fa-image"></i>
              <p> Slider
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?= base_url()?>dashboard/slider" class="nav-link <?php if(isset($activePage) && $activePage == 'slider') echo 'active'; ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Slider</p>
                </a>
              </li>
            </ul>
          </li>

          <li class="nav-item has-treeview <?php if(isset($mainPage) && $mainPage == 'users') echo 'menu-open'; ?>">
            <a href="#" class="nav-link <?php if(isset($mainPage) && $mainPage == 'users') echo 'active'; ?>">
              <i class="nav-icon far fa-image"></i>
              <p> Users
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?= base_url()?>dashboard/users" class="nav-link <?php if(isset($activePage) && $activePage == 'users') echo 'active'; ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Users</p>
                </a>
              </li>
            </ul>
          </li>

          <li class="nav-item has-treeview <?php if(isset($mainPage) && $mainPage == 'categories') echo 'menu-open'; ?>">
            <a href="#" class="nav-link <?php if(isset($mainPage) && $mainPage == 'categories') echo 'active'; ?>">
              <i class="nav-icon fas fa-th"></i>
              <p> Categories
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?= base_url()?>dashboard/categories" class="nav-link <?php if(isset($activePage) && $activePage == 'categories') echo 'active'; ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Categories</p>
                </a>
              </li>
            </ul>
          </li>

          <li class="nav-item has-treeview <?php if(isset($mainPage) && $mainPage == 'collections') echo 'menu-open'; ?>">
            <a href="#" class="nav-link <?php if(isset($mainPage) && $mainPage == 'collections') echo 'active'; ?>">
              <i class="nav-icon fas fa-th"></i>
              <p> Collections
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?= base_url()?>dashboard/collections" class="nav-link <?php if(isset($activePage) && $activePage == 'collections') echo 'active'; ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Collections</p>
                </a>
              </li>
            </ul>
          </li>

          <li class="nav-item has-treeview <?php if(isset($mainPage) && $mainPage == 'products') echo 'menu-open'; ?>">
            <a href="#" class="nav-link <?php if(isset($mainPage) && $mainPage == 'products') echo 'active'; ?>">
              <i class="nav-icon fas fa-book"></i>
              <p>
                Products
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?= base_url()?>dashboard/products" class="nav-link <?php if(isset($activePage) && $activePage == 'products') echo 'active'; ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Products</p>
                </a>
              </li>

              <li class="nav-item">
                <a href="<?= base_url()?>dashboard/blinds" class="nav-link <?php if(isset($activePage) && $activePage == 'blinds') echo 'active'; ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Blinds</p>
                </a>
              </li>
            </ul>
          </li>

        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>