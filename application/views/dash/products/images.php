<section class="content">
  <div class="container-fluid">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- jquery validation -->
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title"><?=$activePage?></small></h3>
          </div>
          <!-- /.card-header -->
          <!-- form start -->
          <form class="form-horizontal" method="POST" action="<?=base_url()?>dashboard/products/images/<?=$productId?>" enctype="multipart/form-data">
            <div class="card-body">
              
              <div class="form-group row">
                <label for="inputFirst Name3" class="col-sm-2 col-form-label">Image</label>
                <div class="col-sm-10">
                  <input type="file" multiple  id="fileInput" class="form-control"  placeholder="Image" name="file[]" value="">
                </div>
              </div>

              <div class="form-group row">
                <label for="inputFirst Name3" class="col-sm-2 col-form-label"></label>
                <div class="col-sm-10">
                    <div class="previewImages"></div>
                </div>
              </div>
          
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
              <button type="submit" class="btn btn-info">Submit</button>
            </div>
            <!-- /.card-footer -->
          </form>
        </div>
        <!-- /.card -->
        </div>
      <!--/.col (left) -->
      <!-- right column -->
      <div class="col-md-6">

      </div>
      <!--/.col (right) -->
    </div>


    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- jquery validation -->
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Images</small></h3>
          </div>
          <!-- /.card-header -->
          <!-- form start -->
            <div class="card-body">
              

              <div class="form-group row">
                <label for="inputFirst Name3" class="col-sm-2 col-form-label"></label>
                <div class="col-sm-10">
                  <?php  foreach ($images as $image) { ?>
                      <div  class="img-wraps">
                        <a href="<?=base_url('dashboard/products/deleteImage/'.$productId.'/'.$image->id)?>"><span  class="closes" title="Delete">&times;</span>
                        <img src="<?=isset($image->image)?base_url().'assets/images/site/products/'.$image->image:'display: none;'?>" style=" width: 200px; margin-top: 20px;">
                      </div>
                  <?php } ?>
                </div>
              </div>
          
            </div>
            <!-- /.card-footer -->
        </div>
        <!-- /.card -->
        </div>
      <!--/.col (left) -->
      <!-- right column -->
      <div class="col-md-6">

      </div>
      <!--/.col (right) -->
    </div>
    <!-- /.row -->
  </div><!-- /.container-fluid -->
</section>

<style type="text/css">
  .img-wraps{
   position: relative;
    display: inline-block;
   
    font-size: 0;
}
.img-wraps .closes {
    position: absolute;
    top: 5px;
    right: 8px;
    z-index: 100;
    background-color: #FFF;
    padding: 4px 3px;
    
    color: #000;
    font-weight: bold;
    cursor: pointer;
   
    text-align: center;
    font-size: 22px;
    line-height: 10px;
    border-radius: 50%;
    border:1px solid red;
}
.img-wraps:hover .closes {
    opacity: 1;
}
</style>


<script type="text/javascript">
  $(function() {
    // Multiple images preview in browser
    var imagesPreview = function(input, placeToInsertImagePreview) {

        if (input.files) {
            var filesAmount = input.files.length;

            for (i = 0; i < filesAmount; i++) {
                var reader = new FileReader();

                reader.onload = function(event) {
                    $($.parseHTML('<img>')).attr('src', event.target.result).attr('style', 'width:200px; margin-top: 20px;').appendTo(placeToInsertImagePreview);
                }

                reader.readAsDataURL(input.files[i]);
            }
        }

    };

    $('#fileInput').on('change', function() {
        imagesPreview(this, 'div.previewImages');
    });
});
</script>