<section class="content">
  <div class="container-fluid">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- jquery validation -->
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title"><?=$activePage?></small></h3>
          </div>
          <!-- /.card-header -->
          <!-- form start -->
          <form class="form-horizontal" method="POST" action="<?=base_url()?>dashboard/blinds/<?=isset($blind)?'edit_save/'.$blind->id:'add'?>" enctype="multipart/form-data">
            <div class="card-body">
              
              <div class="form-group row">
                <label for="inputEmail3" class="col-sm-2 col-form-label">Title</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" placeholder="EN Title" name="title_en" value="<?=isset($blind)?$blind->title_en:set_value('title_en')?>">
                </div>
              </div>
              
               <div class="form-group row">
                <label for="inputEmail3" class="col-sm-2 col-form-label">Price</label>
                <div class="col-sm-10">
                  <input type="number" class="form-control" step="0.0001" placeholder="Price" name="price" value="<?=isset($blind)?$blind->price:set_value('price')?>">
                </div>
              </div>

               <div class="form-group row">
                <label for="inputEmail3" class="col-sm-2 col-form-label">First Discount</label>
                <div class="col-sm-10">
                  <input type="number" class="form-control" step="0.0001" placeholder="Discount 1" name="discount1" value="<?=isset($blind)?$blind->discount1:set_value('discount1')?>">
                </div>
              </div>

              <div class="form-group row">
                <label for="inputEmail3" class="col-sm-2 col-form-label">Second Discount</label>
                <div class="col-sm-10">
                  <input type="number" class="form-control" step="0.0001" placeholder="Discount 2" name="discount2" value="<?=isset($blind)?$blind->discount2:set_value('discount2')?>">
                </div>
              </div>

              <div class="form-group row">
                <label for="inputEmail3" class="col-sm-2 col-form-label">Third Discount</label>
                <div class="col-sm-10">
                  <input type="number" class="form-control" step="0.0001" placeholder="Discount 3" name="discount3" value="<?=isset($blind)?$blind->discount3:set_value('discount3')?>">
                </div>
              </div>

              <div class="form-group row">
                <label for="inputEmail3" class="col-sm-2 col-form-label">Forth Discount</label>
                <div class="col-sm-10">
                  <input type="number" class="form-control" step="0.0001" placeholder="Discount 4" name="discount4" value="<?=isset($blind)?$blind->discount4:set_value('discount4')?>">
                </div>
              </div>

              <div class="form-group row">
                <label for="inputPassword3" class="col-sm-2 col-form-label">Active</label>
                <div class="col-sm-10">
                <input type="checkbox" name="active" <?=(isset($blind) && $blind->active == 1)?'checked':''?> data-bootstrap-switch data-off-color="danger" data-on-color="success">
                 
                </div>
              </div>


              <div class="form-group row">
                <label for="inputFirst Name3" class="col-sm-2 col-form-label">Image</label>
                <div class="col-sm-10">
                  <input type="file" id="fileInput" class="form-control"  placeholder="Image" name="file" value="">
                  <img id="previewImage" src="<?=isset($blind->image)?base_url().'assets/images/site/blinds/'.$blind->image:'display: none;'?>" style="<?=isset($blind->image)?'':'display: none;'?> width: 200px; margin-top: 20px;">
                </div>
              </div>

            </div>
            <!-- /.card-body -->
            <div class="card-footer">
              <button type="submit" class="btn btn-info">Submit</button>
            </div>
            <!-- /.card-footer -->
          </form>
        </div>
        <!-- /.card -->
        </div>
      <!--/.col (left) -->
      <!-- right column -->
      <div class="col-md-6">

      </div>
      <!--/.col (right) -->
    </div>
    <!-- /.row -->
  </div><!-- /.container-fluid -->
</section>


<script type="text/javascript">
  function readURL(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      
      reader.onload = function(e) {
        $('#previewImage').attr('src', e.target.result);
        $('#previewImage').show();
      }
      
      reader.readAsDataURL(input.files[0]);
    }
}

$("#fileInput").change(function() {
  readURL(this);
});

</script>