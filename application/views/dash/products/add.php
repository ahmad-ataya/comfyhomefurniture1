<section class="content">
  <div class="container-fluid">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- jquery validation -->
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title"><?=$activePage?></small></h3>
          </div>
          <!-- /.card-header -->
          <!-- form start -->
          <form class="form-horizontal" method="POST" action="<?=base_url()?>dashboard/products/<?=isset($product)?'edit_save/'.$product->id:'add'?>" enctype="multipart/form-data">
            <div class="card-body">
              
              <div class="form-group row">
                <label for="inputEmail3" class="col-sm-2 col-form-label"> Category</label>
                <div class="col-sm-10">
                  <select class="form-control select2 " name="cat_id" style="height: 20px">
                      <option value="0">Choose One...</option>
                      <?php foreach ($categories as $cat) { ?>
                        <option value="<?=$cat->id?>" <?= (isset($product) && $product->cat_id == $cat->id)?'selected':''?> ><?=$cat->title_en; ?></option>
                      <?php } ?>
                  </select>
                </div>
              </div>

              <div class="form-group row">
                <label for="inputEmail3" class="col-sm-2 col-form-label">Route</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" placeholder="Route" name="route" value="<?=isset($product)?$product->route:set_value('route')?>">
                </div>
              </div>

              
              <div class="form-group row">
                <label for="inputEmail3" class="col-sm-2 col-form-label">EN Title</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" placeholder="EN Title" name="title_en" value="<?=isset($product)?$product->title_en:set_value('title_en')?>">
                </div>
              </div>
              

              <div class="form-group row">
                <label for="inputEmail3" class="col-sm-2 col-form-label">FR Title</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" placeholder="FR Title" name="title_fr" value="<?=isset($product)?$product->title_fr:set_value('title_fr')?>">
                </div>
              </div>

              <div class="form-group row">
                <label for="inputEmail3" class="col-sm-2 col-form-label">EN Short Desc</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" placeholder="En Short Desc" name="short_desc_en" value="<?=isset($product)?$product->short_desc_en:set_value('short_desc_en')?>">
                </div>
              </div>
              

              <div class="form-group row">
                <label for="inputEmail3" class="col-sm-2 col-form-label">FR Short Desc</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" placeholder="FR Short Desc" name="short_desc_fr" value="<?=isset($product)?$product->short_desc_fr:set_value('short_desc_fr')?>">
                </div>
              </div>

              <div class="form-group row">
                <label for="inputEmail3" class="col-sm-2 col-form-label">EN Description</label>
                <div class="col-sm-10">
                  <textarea class="form-control" placeholder="EN Description" name="desc_en" rows="5" ><?=isset($product)?$product->desc_en:set_value('desc_en')?></textarea>
                </div>
              </div>

              <div class="form-group row">
                <label for="inputEmail3" class="col-sm-2 col-form-label">FR Description</label>
                <div class="col-sm-10">
                  <textarea class="form-control" placeholder="EN Description" name="desc_fr" rows="5" ><?=isset($product)?$product->desc_en:set_value('desc_en')?></textarea>
                </div>
              </div>

               <div class="form-group row">
                <label for="inputEmail3" class="col-sm-2 col-form-label">Price</label>
                <div class="col-sm-10">
                  <input type="number" class="form-control" placeholder="Price" name="price" value="<?=isset($product)?$product->price:set_value('price')?>">
                </div>
              </div>

               <div class="form-group row">
                <label for="inputEmail3" class="col-sm-2 col-form-label">Discount</label>
                <div class="col-sm-10">
                  <input type="number" class="form-control" placeholder="Discount" name="discount" value="<?=isset($product)?$product->discount:set_value('discount')?>">
                </div>
              </div>

              <div class="form-group row">
                <label for="inputPassword3" class="col-sm-2 col-form-label">Size</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" placeholder="Size" name="size" value="<?=isset($product)?$product->size:set_value('size')?>">
                </div>
              </div>

              <div class="form-group row">
                <label for="inputPassword3" class="col-sm-2 col-form-label">Color</label>
                <div class="col-sm-10 input-group my-colorpicker2">
                  <input class="btn btn-warning" type="button" id="addColor" value="Add Color"/>
                  <?php if(isset($product)) { ?>
                      <?php foreach($product->color as $color){ ?> 
                        <input type="color" class="form-control" name="color[]" value="<?=$color?>"><div class="input-group-append"><span class="input-group-text"><i class="fa fa-trash removeColor"></i></span></div>
                      <?php } ?>
                  <?php } ?>

                </div>
              </div>

              <div class="form-group row">
                <label for="inputPassword3" class="col-sm-2 col-form-label">Material</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" placeholder="Material" name="material" value="<?=isset($product)?$product->material:set_value('material')?>">
                </div>
              </div>

              <div class="form-group row">
                <label for="inputPassword3" class="col-sm-2 col-form-label">Weight</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" placeholder="Weight" name="weight" value="<?=isset($product)?$product->weight:set_value('weight')?>">
                </div>
              </div>



              <div class="form-group row">
                <label for="inputPassword3" class="col-sm-2 col-form-label">In Stock</label>
                <div class="col-sm-10">
                <!-- <input type="checkbox" name="in_stock" <?=(isset($product) && $product->in_stock == 1)?'checked':''?> data-bootstrap-switch data-off-color="danger" data-on-color="success"> -->
                <div class="row">
                  <div class="col-md-6">
                    <select class="form-control select2 " name="in_stock" style="height: 20px" id="in_stock">
                      <option value="0">Choose One...</option>
                        <option value="1" <?= (isset($product) && $product->in_stock == 1)?'selected':''?> >In Stock</option>
                        <option value="2" <?= (isset($product) && $product->in_stock == 2)?'selected':''?> >Due back in stock</option>
                    </select>
                  </div>
                  <div class="col-md-6" id="due_stock" style="<?= ($product->in_stock !=2)?'display: none':''?>;">
                    <input type="text" name="due_stock" class="form-control" value="<?=isset($product)?$product->due_stock:set_value('due_stock')?>">
                  </div>
                </div>
                </div>
              </div>

              <div class="form-group row">
                <label for="inputPassword3" class="col-sm-2 col-form-label">Most Selling</label>
                <div class="col-sm-10">
                <input type="checkbox" name="is_most_selling" <?=(isset($product) && $product->is_most_selling == 1)?'checked':''?> data-bootstrap-switch data-off-color="danger" data-on-color="success">
                 
                </div>
              </div>

              <div class="form-group row">
                <label for="inputPassword3" class="col-sm-2 col-form-label">New Collection</label>
                <div class="col-sm-10">
                <input type="checkbox" name="is_new_collection" <?=(isset($product) && $product->is_new_collection == 1)?'checked':''?> data-bootstrap-switch data-off-color="danger" data-on-color="success">
                 
                </div>
              </div>


              <div class="form-group row">
                <label for="inputFirst Name3" class="col-sm-2 col-form-label">Image</label>
                <div class="col-sm-10">
                  <input type="file" id="fileInput" class="form-control"  placeholder="Image" name="file" value="">
                  <img id="previewImage" src="<?=isset($product->image)?base_url().'assets/images/site/products/'.$product->image:'display: none;'?>" style="<?=isset($product->image)?'':'display: none;'?> width: 200px; margin-top: 20px;">
                </div>
              </div>

            </div>
            <!-- /.card-body -->
            <div class="card-footer">
              <button type="submit" class="btn btn-info">Submit</button>
            </div>
            <!-- /.card-footer -->
          </form>
        </div>
        <!-- /.card -->
        </div>
      <!--/.col (left) -->
      <!-- right column -->
      <div class="col-md-6">

      </div>
      <!--/.col (right) -->
    </div>
    <!-- /.row -->
  </div><!-- /.container-fluid -->
</section>


<script type="text/javascript">
  function readURL(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      
      reader.onload = function(e) {
        $('#previewImage').attr('src', e.target.result);
        $('#previewImage').show();
      }
      
      reader.readAsDataURL(input.files[0]);
    }
}

$("#fileInput").change(function() {
  readURL(this);
});

$("#in_stock").change(function() {
  if($('#in_stock').val() == 2)
    $('#due_stock').show();
  else
    $('#due_stock').hide();
});

$('#addColor').click(function(e){
  $('.my-colorpicker2').append('<input type="color" class="form-control" name="color[]"><div class="input-group-append"><span class="input-group-text"><i class="fa fa-trash removeColor"></i></span></div>');
})


$( "body" ).on( "click", ".removeColor", function() {
  $(this).parent().parent().siblings()[1].remove();
  $(this).parent().parent().remove();
})

</script>