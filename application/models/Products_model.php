<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Products_model extends CI_Model
{
	public function __construct()
	{
	    parent::__construct();
	}

	public function getProductsOfCategories($ids = array()){
		$this->db->where_in('cat_id',$ids);

	    $this->db->order_by('rand()');
		// $this->db->limit(10);
		
		return $this->db->get('products')->result();
	}

	public function getMostSellingProducts($limit = 10){
		$this->db->where('is_most_selling',1);
	    $this->db->order_by('rand()');
		// $this->db->limit($limit);
		
		return $this->db->get('products')->result();
	}

	public function getNewCollectionProducts($limit = 10){
		$this->db->where('is_new_collection',1);
	    $this->db->order_by('rand()');
		// $this->db->limit($limit);
		
		return $this->db->get('products')->result();
	}

	public function getAllProducts($productId =-1){
		$this->db->select('products.*, categories.title_en as category_title_en');
		if($productId != -1)
			$this->db->where('products.id',$productId);
		$this->db->join('categories','categories.id = products.cat_id','left');
		
		if($productId != -1)
			return $this->db->get('products')->row();
		
		$this->db->order_by('id','desc');
		return $this->db->get('products')->result();
	}

	public function addProduct($data){
		return $this->db->insert('products',$data);
	}

	public function deleteProduct($productId){
		$this->db->where('id',$productId);
		$this->db->delete('products');
	}

	public function updateProduct($productId,$data){
		$this->db->where('id',$productId);
		return $this->db->update('products',$data);
	}

	public function getProductInfo($id,$route = ''){
		$this->db->select('products.*,categories.route as categoryRoute, categories.title_fr as category_title_fr, categories.title_fr as category_title_fr, categories.title_en as category_title_en');
		$this->db->join('categories','categories.id = products.cat_id');
		if($id == 0)
		    $this->db->where('products.route',$route);
		else
		    $this->db->where('products.id',$id);
		return $this->db->get('products')->row();
	}

	public function getProductsOfCategory($route = '',$catId = -1){
		$this->db->select('products.*');
		if($route != ''){
			$this->db->join('categories','categories.id = products.cat_id');
			$this->db->like('categories.route',$route);
		}
		if($catId != -1){
			if(!is_array($catId))
				$catId = array($catId);
			$this->db->join('categories','categories.id = products.cat_id');
			$this->db->where_in('products.cat_id',$catId);
			$this->db->or_where_in('categories.parent_id',$catId);
		}

		$this->db->order_by('rand()');
				
		return $this->db->get('products')->result();
	}

	public function getProductImages($id){
		$this->db->where('product_id',$id);

		return $this->db->get('product_images')->result();
	}

	public function deleteImage($productId,$imageId){
		$this->db->where('id',$imageId);
		$this->db->where('product_id',$productId);
		return $this->db->delete('product_images');
	}

	public function addProductImage($productId,$fileName){
		return $this->db->insert('product_images',array('product_id' => $productId, 'image' => $fileName));
	}
}