<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Users_model extends CI_Model
{
	public function __construct()
	{
	    parent::__construct();
	}

	public function getAllUsers($rowId = -1){
		if($rowId != -1)
			$this->db->where('id',$rowId);

		$query = $this->db->get('users');
		if($rowId != -1)
			return  $query->row();
		return $query->result();

	}

}