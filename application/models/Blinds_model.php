<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Blinds_model extends CI_Model
{
	public function __construct()
	{
	    parent::__construct();
	}

	public function getAllBlinds($blindId =-1,$getNotActive = fale){
		if($blindId != -1)
			$this->db->where('blinds.id',$blindId);
		
		if(!$getNotActive)
		 $this->db->where('active',1);

		if($blindId != -1)
			return $this->db->get('blinds')->row();

		$this->db->order_by('rand()');
		return $this->db->get('blinds')->result();
	}

	public function getBlind($id = false, $route = false){
		$this->db->where('id',$id);
		return $this->db->get('blinds')->row();
	}

	public function addBlind($data){
		return $this->db->insert('blinds',$data);
	}

	public function deleteBlind($blindId){
		$this->db->where('id',$blindId);
		$this->db->delete('blinds');
	}

	public function updateBlind($blindId,$data){
		$this->db->where('id',$blindId);
		return $this->db->update('blinds',$data);
	}
}