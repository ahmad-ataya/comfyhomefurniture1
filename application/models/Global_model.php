<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Ion Auth Model
 * @property Ion_auth $ion_auth The Ion_auth library
 */
class Global_model extends CI_Model
{
	public function __construct()
	{
	    parent::__construct();
	}

	public function getAllCategories($active = -1,$parentId = -1){
		if($active != -1)
			$this->db->where('active',$active);

		if($parentId != -1)
			$this->db->where('parent_id',$parentId);
		
		return $this->db->get('categories')->result();
	}

	public function getAllSliders($id = -1){
		if($id != -1)
			$this->db->where('id',$id);

		if($id != -1)
			return $this->db->get('sliders')->row();

		return $this->db->get('sliders')->result();
	}

	public function addSlider($data){
		$this->db->insert('sliders',$data);
	}

	public function deleteSlider($sliderId){
		$this->db->where('id',$sliderId);
		$this->db->delete('sliders');
	}

}