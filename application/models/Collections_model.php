<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Collections_model extends CI_Model
{
	public function __construct()
	{
	    parent::__construct();
	}

	public function getCollection($id = false, $route = false){
		if($id)
			$this->db->where('id',$id);
		if($route)
			$this->db->where('route',$route);
		
		return $this->db->get('collections')->row();
	}

	public function getCollectionCategories($collectionId){
		$this->db->join('categories','categories.id = collections_categories.category_id');		
		
		$this->db->where('collection_id',$collectionId);
		return $this->db->get('collections_categories')->result();
	}

	public function getAllCollections($collectionId = -1,$active = -1){
		if($collectionId != -1)
			$this->db->where('collections.id',$collectionId);
		
		if($active != -1)
			$this->db->where('active',$active);
		
		if($collectionId != -1)
			return $this->db->get('collections')->row();
		return $this->db->get('collections')->result();
	}

	public function addCollection($data){
		return $this->db->insert('collections',$data);
	}

	public function insertCategoriesCollection($data){
		return $this->db->insert_batch('collections_categories',$data);
	}

	public function deleteCollection($collectionId){
		$this->db->where('id',$collectionId);
		$this->db->delete('collections');
	}

	public function updateCollection($collectionId,$data){
		$this->db->where('id',$collectionId);
		return $this->db->update('collections',$data);
	}

	public function updateCollectionCategories($collectionId,$categories){
		$this->db->where('collection_id',$collectionId);
		$this->db->delete('collections_categories');
		$categoriesCollection = array();
		foreach ($categories as $cat) {
        	$categoriesCollection[] = array(
        		'collection_id' => $collectionId,
        		'category_id' => $cat,
        	);
        }
        $this->insertCategoriesCollection($categoriesCollection);
	}
}