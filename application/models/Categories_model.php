<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Categories_model extends CI_Model
{
	public function __construct()
	{
	    parent::__construct();
	}

	public function getCategory($id = false, $route = false){
		if($id)
			$this->db->where('id',$id);
		if($route)
			$this->db->where('route',$route);
		
		return $this->db->get('categories')->row();
	}

	public function getAllCategories($catId =-1){
		$this->db->select('categories.*, parent.title_en as parent_title_en');
		if($catId != -1)
			$this->db->where('categories.id',$catId);
		$this->db->join('categories as parent','categories.parent_id = parent.id','left');
		
		if($catId != -1)
			return $this->db->get('categories')->row();
		return $this->db->get('categories')->result();
	}

	public function addCategory($data){
		return $this->db->insert('categories',$data);
	}

	public function deleteCategory($catId){
		$this->db->where('id',$catId);
		$this->db->delete('categories');
	}

	public function updateCategory($catId,$data){
		$this->db->where('id',$catId);
		return $this->db->update('categories',$data);
	}
}