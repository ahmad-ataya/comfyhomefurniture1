<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class MY_site extends CI_Controller {
	public $data; 
	public function __construct(){
	    parent::__construct();

		$this->load->model('global_model');
		$this->load->model('collections_model');
		$this->data['langCode'] = 'en';
		$allCategoriesArray = $this->global_model->getAllCategories(1);
		$allCategories = array();
		foreach ($allCategoriesArray as $category) {
			$allCategories[$category->parent_id][] = $category;
			$categoriesInfo[$category->id] = $category;
		}

		$this->data['collections'] = $this->collections_model->getAllCollections(-1,1);
		$this->data['allCategories'] = $allCategories;
		$this->data['categoriesInfo'] = $categoriesInfo;
		$this->data['categories'] = $allCategories[0];
		$this->data['sliders'] = $this->global_model->getAllSliders();
	}
}

class MY_dash extends CI_Controller {
	public $data; 
	public function __construct(){
	    parent::__construct();

		$this->load->model('global_model');
		$this->data['langCode'] = 'en';

		if(!$this->ion_auth->logged_in())
			redirect('auth/login');

		$this->data['user'] = $this->ion_auth->user()->row();
		// $allCategories = array();
		// foreach ($allCategoriesArray as $category) {
		// 	$allCategories[$category->parent_id][] = $category;
		// 	$categoriesInfo[$category->id] = $category;
		// }

		// $this->data['allCategories'] = $allCategories;
		// $this->data['categoriesInfo'] = $categoriesInfo;
		// $this->data['categories'] = $allCategories[0];
		// $this->data['sliders'] = $this->global_model->getAllSliders();
	}
}
