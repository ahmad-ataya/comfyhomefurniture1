<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_site {

	public function __construct(){
	    parent::__construct();
	    $this->load->model('products_model');
	    $this->load->model('collections_model');
	}

	public function index()
	{
		$this->data['rightSliderProducts'] = $this->products_model->getProductsOfCategories(array(17,23,25,29));
		$this->data['leftSliderProducts'] = $this->products_model->getProductsOfCategories(array(14));
		$this->data['mostSellingProducts'] = $this->products_model->getMostSellingProducts();
		// $this->data['collections'] = $this->collections_model->getAllCollections(-1,true);
		$this->data['content'] = $this->load->view('site/home',$this->data,TRUE);
		$this->load->view('site/index',$this->data);
	}


	public function test()
	{
		var_dump($_SERVER); die();
	}

	public function aboutUs(){
		$this->data['content'] = $this->load->view('site/about_us',$this->data,TRUE);
		$this->load->view('site/index',$this->data);	
	}

	public function contactUS(){
		$userName = strip_tags($this->input->post('userName'));
		$userEmail = strip_tags($this->input->post('userEmail'));
		$userSubject = strip_tags($this->input->post('userSubject'));
		$userMessage = strip_tags($this->input->post('userMessage'));

		$mail = $this->load->view('contactUsMail_view',array('name' => $userName, 'email' => $userEmail, 'subject' => $userSubject, 'message' => $userMessage),true);
		$this->sendEmail('info@comfyhomefurniture.com',$userSubject,$mail);
		$this->sendEmail('ahmad.3taya@gmail.com',$userSubject,$mail);

		redirect('');
	}

	private function  sendEmail($toEmail,$subject,$message){
		$this->load->library('email');
		$config = array(
		    'protocol' => 'mail', // 'mail', 'sendmail', or 'smtp'
		    'smtp_host' => 'mail.comfyhomefurniture.com', 
		    'smtp_port' => 587,
		    'smtp_user' => 'contact@example.com',
		    'smtp_pass' => 'NGW#zta=D#O9',
		    // 'smtp_crypto' => 'tls', //can be 'ssl' or 'tls' for example
		    'mailtype' => 'html', //plaintext 'text' mails or 'html'
		    'smtp_timeout' => '4', //in seconds
		    'CRLF' => '\r\n',
		    'newline' => '\r\n',
		    'new_line' => '\r\n',
		    'charset' => 'utf-8',
		    'wordwrap' => TRUE
		);

		$this->email->initialize($config);

		$this->email->from('contact@comfyhomefurniture.com', 'Contact US');

		$this->email->to($toEmail);
		$this->email->subject($subject);
		$this->email->message($message);

		// echo $this->email->send();
		// echo $this->email->print_debugger();
	}
}
