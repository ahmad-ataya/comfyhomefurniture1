<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blinds extends MY_site {

	public function __construct(){
	    parent::__construct();
	    $this->load->model('blinds_model');
	    // $this->load->model('collections_model');
	    // $this->load->model('products_model');
	}

	public function index()
	{
		$this->data['blinds'] = $this->blinds_model->getAllBlinds();
		
		$this->data['content'] = $this->load->view('site/blinds_view',$this->data,TRUE);
		$this->load->view('site/index',$this->data);
	}
}
