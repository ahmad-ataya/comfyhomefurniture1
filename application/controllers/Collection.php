<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Collection extends MY_site {

	public function __construct(){
	    parent::__construct();
	    $this->load->model('categories_model');
	    $this->load->model('products_model');
	    $this->load->model('collections_model');
	}

	public function index($route = '')
	{
		if($route == 'new')
			return $this->new();
		$collection = $this->collections_model->getCollection(false,$route);
		if(!$collection)
			redirect('','refresh');
		$this->data['collection'] = $collection;
		$categories = $this->collections_model->getCollectionCategories($collection->id);
		$categoriesIds = array();
		foreach ($categories as $cat)
			$categoriesIds[] = $cat->category_id;
		$this->data['products'] = $this->products_model->getProductsOfCategory('',$categoriesIds);
		$this->data['isCollection'] = true;
		$this->data['content'] = $this->load->view('site/collection_view',$this->data,TRUE);
		$this->load->view('site/index',$this->data);
	}


	public function new(){

		$this->data['category'] = (object)array(
			'isNewCollection' => true, 
			'route' => base_url().'collection/new', 
			'image' => '21.jpg',
			'title_en' => 'New Collection',
			'title_fr' => 'New Collection'
		);

		$this->data['products'] = $this->products_model->getNewCollectionProducts();
		
		$this->data['content'] = $this->load->view('site/category_view',$this->data,TRUE);
		$this->load->view('site/index',$this->data);
	}

}
