<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Collections extends MY_dash {

	public function __construct(){
	    parent::__construct();
	    $this->load->model('collections_model');
	    $this->load->model('categories_model');
		$this->data['mainPage'] = $this->data['activePage'] = 'collections'; 
	}

	public function index($route = '')
	{
		$this->data['tableKeys'] = array('id' => 'ID','title_en' => 'En Title','title_fr' => 'FR Title','image' => 'Imaeg','active' => 'active','actions' => 'Actions');
		$this->data['actionsValue'] = "<a href='".base_url()."dashboard/collections/edit/{ID}' class='btn btn-info' style='margin-right: 10px;'>Edit</a><a href='".base_url()."dashboard/collections/delete/{ID}' class='btn btn-danger'>Remove</a>";
		$this->data['tableData'] = $this->collections_model->getAllCollections();
		$this->data['keySpecific'] = array('image' => '<img  src="'.base_url().'assets/images/site/collections/{value}"   width="200">');
		$this->data['addPath'] = 'dashboard/collections/add';

		$this->data['content'] = $this->load->view('dash/table_view',$this->data,TRUE);
		$this->load->view('dash/index',$this->data);
	}

	public function edit($collectionId){
		$collection = $this->collections_model->getAllCollections($collectionId);

		if(!$collection)
			return redirect('dashboard/collections');

		$this->data['collection'] = $collection;

		$collectionsCategories =$this->collections_model->getCollectionCategories($collectionId); 

		foreach ($collectionsCategories as $cat) {
			$collectionCategories[] = $cat->category_id;
		}
		$this->data['collectionCategories'] = $collectionCategories;
		$this->data['categories'] = $this->categories_model->getAllCategories();
		$this->data['content'] = $this->load->view('dash/collections/add',$this->data,TRUE);
		$this->load->view('dash/index',$this->data);
	}

	public function edit_save($collectionId){
		$collection = $this->collections_model->getAllCollections($collectionId);

		if(!$collection || !$_POST)
			return redirect('dashboard/collections');

		$fileName = $collection->image;
		if (isset($_FILES) && count($_FILES) > 0){
			$config['upload_path']          = 'assets/images/site/collections';
	        $config['allowed_types']        = 'gif|jpg|png|jpeg';

	        $this->load->library('upload', $config);

	        if($this->upload->do_upload('file')){
	                $fileData =$this->upload->data();
                $fileName = $fileData['file_name'];

	        }
		}


        $title_en = $this->input->post('title_en');
        $title_fr = $this->input->post('title_fr');
        $price = $this->input->post('price');
        $active = $this->input->post('active');
        $route = $this->input->post('route');
        $categories = $this->input->post('categories');
        if(!$categories){
    		$this->session->set_flashdata('messageError', 'categories are required'); 
			return redirect("dashboard/collections/edit/$collectionId", 'refresh');
    	}else{
	        $data = array('title_en' => $title_en, 'title_fr' => $title_fr, 'price' => $price, 'active' => ($active)?1:0, 'route' => $route, 'image' => $fileName);
	        $this->collections_model->updateCollection($collectionId,$data);
	        $this->collections_model->updateCollectionCategories($collectionId,$categories);
	        $this->session->set_flashdata('message', 'inserted successfully');
			return redirect("dashboard/collections", 'refresh');
		}
	}

	public function add(){
		if($_POST){
			if (isset($_FILES) && count($_FILES) > 0){
				$config['upload_path']          = 'assets/images/site/collections';
		        $config['allowed_types']        = 'gif|jpg|png|jpeg';

		        $this->load->library('upload', $config);

		        if ( ! $this->upload->do_upload('file'))
	                $this->session->set_flashdata('messageError', $this->upload->display_errors());
		        else{
		            $fileData =$this->upload->data();
	                $fileName = $fileData['file_name'];

	                $title_en = $this->input->post('title_en');
	                $title_fr = $this->input->post('title_fr');
	                $price = $this->input->post('price');
	                $active = $this->input->post('active');
	                $route = $this->input->post('route');
	                $categories = $this->input->post('categories');
	                if(!$categories)
	            		$this->session->set_flashdata('messageError', 'categories are required');    	
	                else{
		                $this->collections_model->addCollection(array('title_en' => $title_en, 'title_fr' => $title_fr, 'price' => $price, 'active' => ($active)?1:0, 'route' => $route, 'image' => $fileName));
		                $collectionId = $this->db->insert_id();
		                $categoriesCollection = array();
		                foreach ($categories as $cat) {
		                	$categoriesCollection[] = array(
		                		'collection_id' => $collectionId,
		                		'category_id' => $cat,
		                	);
		                }
		                $this->collections_model->insertCategoriesCollection($categoriesCollection);

		                $this->session->set_flashdata('message', 'inserted successfully');
						redirect("dashboard/collections", 'refresh');
	                }
		        }
			}
			else
                $this->session->set_flashdata('messageError', 'Image Required');

		}
		$this->data['categories'] = $this->categories_model->getAllCategories();
		$this->data['content'] = $this->load->view('dash/collections/add',$this->data,TRUE);
		$this->load->view('dash/index',$this->data);
	}

	public function delete($collectionId){
		$this->collections_model->deleteCollection($collectionId);

		$this->session->set_flashdata('message', 'removed successfully');
		redirect("dashboard/collections", 'refresh');
	}
}
