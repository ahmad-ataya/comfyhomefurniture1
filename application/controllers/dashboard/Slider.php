<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Slider extends MY_dash {

	public function __construct(){
	    parent::__construct();
	    $this->load->model('global_model');
		$this->data['mainPage'] = $this->data['activePage'] = 'slider'; 
	}

	public function index($route = '')
	{
		$this->data['tableKeys'] = array('id' => 'ID','link' => 'Link','description' => 'Description','image' => 'Image','actions' => 'Actions');
		$this->data['actionsValue'] = "<a href='".base_url()."dashboard/slider/edit/{ID}' class='btn btn-info' style='margin-right: 10px;'>Edit</a><a href='".base_url()."dashboard/slider/delete/{ID}' class='btn btn-danger'>Remove</a>";

		$this->data['tableData'] = $this->global_model->getAllSliders();
		$this->data['keySpecific'] = array('image' => '<img  src="'.base_url().'assets/images/site/slider/{value}"/     width="200">','link' => '<a href="'.base_url().'/{value}" target="_blank">{value}</a>');
		$this->data['addPath'] = 'dashboard/slider/add';

		$this->data['content'] = $this->load->view('dash/table_view',$this->data,TRUE);
		$this->load->view('dash/index',$this->data);
	}

	public function edit($sliderId){
		$slider = $this->global_model->getAllSliders($sliderId);

		if(!$slider)
			return redirect('dashboard/slider');

		$this->data['slider'] = $slider;
		
		$this->data['content'] = $this->load->view('dash/slider/add',$this->data,TRUE);
		$this->load->view('dash/index',$this->data);
	}

	public function edit_save($sliderId){
		$slider = $this->global_model->getAllSliders($sliderId);

		if(!$slider)
			return redirect('dashboard/slider');

		// $link = $this->inpu
		
		$this->data['content'] = $this->load->view('dash/slider/add',$this->data,TRUE);
		$this->load->view('dash/index',$this->data);
	}

	public function add(){
		if($_POST){
			if (isset($_FILES) && count($_FILES) > 0){
				$config['upload_path']          = 'assets/images/site/slider';
		        $config['allowed_types']        = 'gif|jpg|png|jpeg';

		        $this->load->library('upload', $config);

		        if ( ! $this->upload->do_upload('file'))
	                $this->session->set_flashdata('messageError', $this->upload->display_errors());
		        else{
		                $fileData =$this->upload->data();
	                $fileName = $fileData['file_name'];

	                $link = $this->input->post('link');
	                $description = $this->input->post('description');
	                $this->global_model->addSlider(array('link' => $link, 'description' => $description, 'image' => $fileName));

	                $this->session->set_flashdata('message', 'inserted successfully');
					redirect("dashboard/slider", 'refresh');
		        }
			}
			else
                $this->session->set_flashdata('messageError', 'Image Required');

		}
		$this->data['content'] = $this->load->view('dash/slider/add',$this->data,TRUE);
		$this->load->view('dash/index',$this->data);
	}

	public function delete($sliderId){
		$this->global_model->deleteSlider($sliderId);

		$this->session->set_flashdata('message', 'removed successfully');
		redirect("dashboard/slider", 'refresh');
	}
}
