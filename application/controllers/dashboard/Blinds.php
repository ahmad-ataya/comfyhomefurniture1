<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blinds extends MY_dash {

	public function __construct(){
	    parent::__construct();
	    $this->load->model('blinds_model');
		$this->data['mainPage'] = 'products' ;
		$this->data['activePage'] = 'blinds'; 
	}



	public function index($route = '')
	{
		$this->data['tableKeys'] = array('id' => 'ID','title_en' => 'Title' ,'image' => 'Image','price' => 'Price','discount1' => 'Discount 1','discount2' => 'Discount 2','discount3' => 'Discount 3','discount4' => 'Discount 4', 'active' => 'active','actions' => 'Actions');
		$this->data['actionsValue'] = "<a href='".base_url()."dashboard/blinds/edit/{ID}' class='btn btn-info' style='margin-right: 10px;'>Edit</a><a href='".base_url()."dashboard/blinds/delete/{ID}' class='btn btn-danger'>Remove</a>";

		$this->data['tableData'] = $this->blinds_model->getAllBlinds(-1,true);
		$this->data['keySpecific'] = array('image' => '<img  src="'.base_url().'assets/images/site/blinds/{value}"   width="200">');
		$this->data['addPath'] = 'dashboard/blinds/add';

		$this->data['content'] = $this->load->view('dash/table_view',$this->data,TRUE);
		$this->load->view('dash/index',$this->data);
	}

	public function edit($catId){
		$blind = $this->blinds_model->getAllBlinds($catId,true);

		if(!$blind)
			return redirect('dashboard/blinds');

		$this->data['blind'] = $blind;
		
		$this->data['content'] = $this->load->view('dash/products/blinds_add',$this->data,TRUE);
		$this->load->view('dash/index',$this->data);
	}

	public function edit_save($catId){
		$blind = $this->blinds_model->getAllBlinds($catId,true);

		if(!$blind || !$_POST)
			return redirect('dashboard/blinds');

		$fileName = $blind->image;
		if (isset($_FILES) && count($_FILES) > 0){
			$config['upload_path']          = 'assets/images/site/blinds';
	        $config['allowed_types']        = 'gif|jpg|png|jpeg';

	        $this->load->library('upload', $config);

	        if($this->upload->do_upload('file')){
	                $fileData =$this->upload->data();
                $fileName = $fileData['file_name'];

	        }
		}


        $title_en = $this->input->post('title_en');
        $title_fr = '';
        $price = $this->input->post('price');
        $discount1 = $this->input->post('discount1');
        $discount2 = $this->input->post('discount2');
        $discount3 = $this->input->post('discount3');
        $discount4 = $this->input->post('discount4');
        $active = $this->input->post('active');


        $data = array('title_en' => $title_en, 'title_fr' => $title_fr, 'price' => $price, 'active' => ($active)?1:0, 'discount1' => $discount1,'discount2' => $discount2,'discount3' => $discount3,'discount4' => $discount4, 'image' => $fileName);
        $this->blinds_model->updateBlind($catId,$data);
        $this->session->set_flashdata('message', 'inserted successfully');
		return redirect("dashboard/blinds", 'refresh');
	}

	public function add(){
		if($_POST){
			if (isset($_FILES) && count($_FILES) > 0){
				$config['upload_path']          = 'assets/images/site/blinds';
		        $config['allowed_types']        = 'gif|jpg|png|jpeg';

		        $this->load->library('upload', $config);

		        if ( ! $this->upload->do_upload('file'))
	                $this->session->set_flashdata('messageError', $this->upload->display_errors());
		        else{
		                $fileData =$this->upload->data();
	                $fileName = $fileData['file_name'];

	                $title_en = $this->input->post('title_en');
	                $title_fr = '';
	                $price = $this->input->post('price');
	                $discount1 = $this->input->post('discount1');
	                $discount2 = $this->input->post('discount2');
	                $discount3 = $this->input->post('discount3');
	                $discount4 = $this->input->post('discount4');
	                $active = $this->input->post('active');

	                $this->blinds_model->addBlind(array('title_en' => $title_en, 'title_fr' => $title_fr, 'price' => $price, 'active' => ($active)?1:0, 'discount1' => $discount1,'discount2' => $discount2,'discount3' => $discount3,'discount4' => $discount4, 'image' => $fileName));

	                $this->session->set_flashdata('message', 'inserted successfully');
					redirect("dashboard/blinds", 'refresh');
		        }
			}
			else
                $this->session->set_flashdata('messageError', 'Image Required');

		}
		$this->data['blinds'] = $this->blinds_model->getAllBlinds(-1,true);
		$this->data['content'] = $this->load->view('dash/products/blinds_add',$this->data,TRUE);
		$this->load->view('dash/index',$this->data);
	}

	public function delete($catId){
		$this->blinds_model->deleteBlind($catId);

		$this->session->set_flashdata('message', 'removed successfully');
		redirect("dashboard/blinds", 'refresh');
	}
}
