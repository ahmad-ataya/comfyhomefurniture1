<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_dash {

	public function __construct(){
	    parent::__construct();
	}

	public function index($route = '')
	{
		$this->data['content'] = $this->load->view('dash/home',$this->data,TRUE);
		$this->load->view('dash/index',$this->data);
	}
}
