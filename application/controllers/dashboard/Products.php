<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends MY_dash {

	public function __construct(){
	    parent::__construct();

	    $this->load->model('categories_model');
	    $this->load->model('products_model');
		$this->data['mainPage'] = $this->data['activePage'] = 'products'; 
	}



	public function index($route = '')
	{
		$this->data['tableKeys'] = array('id' => 'ID','category_title_en' => 'Category', 'route' => 'Route' ,'title_en' => 'En Title','title_fr' => 'FR title','desc_en' => 'En Description','desc_fr' => 'FR Description','price' => 'Price', 'discount' => 'Discount', 'image' => 'Imaeg','in_stock' => 'In Stock','actions' => 'Actions');
		$this->data['actionsValue'] = "<a href='".base_url()."dashboard/products/images/{ID}' class='btn btn-success' style='margin-right: 10px;'>Images</a> <a href='".base_url()."dashboard/products/edit/{ID}' class='btn btn-info' style='margin-right: 10px;'>Edit</a><a href='".base_url()."dashboard/products/delete/{ID}' class='btn btn-danger'>Remove</a>";

		$this->data['tableData'] = $this->products_model->getAllProducts();
		$this->data['keySpecific'] = array('image' => '<img  src="'.base_url().'assets/images/site/products/{value}"   width="200">');
		$this->data['addPath'] = 'dashboard/products/add';

		$this->data['content'] = $this->load->view('dash/table_view',$this->data,TRUE);
		$this->load->view('dash/index',$this->data);
	}

	public function edit($productId){
		$product = $this->products_model->getAllProducts($productId);

		if(!$product)
			return redirect('dashboard/products');

		try {
			$product->color  = json_decode($product->color);
		} catch (Exception $e) {
			$product->color = array();
		}

		$this->data['product'] = $product;
		
		$this->data['categories'] = $this->categories_model->getAllCategories();
		$this->data['content'] = $this->load->view('dash/products/add',$this->data,TRUE);
		$this->load->view('dash/index',$this->data);
	}

	public function edit_save($productId){
		$category = $this->products_model->getAllProducts($productId);

		if(!$category || !$_POST)
			return redirect('dashboard/products');


		$fileName = $category->image;
		if (isset($_FILES) && count($_FILES) > 0){
			$config['upload_path']          = 'assets/images/site/products';
	        $config['allowed_types']        = 'gif|jpg|png|jpeg';

	        $this->load->library('upload', $config);

	        if($this->upload->do_upload('file')){
	                $fileData =$this->upload->data();
                $fileName = $fileData['file_name'];

	        }
		}


        $title_en = $this->input->post('title_en');
        $title_fr = $this->input->post('title_fr');
        $short_desc_en = $this->input->post('short_desc_en');
        $short_desc_fr = $this->input->post('short_desc_fr');
        $desc_en = $this->input->post('desc_en');
        $desc_fr = $this->input->post('desc_fr');
        $cat_id = $this->input->post('cat_id');
        $in_stock = $this->input->post('in_stock');
        $due_stock = $this->input->post('due_stock');
        $price = $this->input->post('price');
        $discount = $this->input->post('discount');
        $route = $this->input->post('route');
        $material = $this->input->post('material');
        $size = $this->input->post('size');
        $color = $this->input->post('color');
        $weight = $this->input->post('weight');
        $is_most_selling = $this->input->post('is_most_selling');
        $is_new_collection = $this->input->post('is_new_collection');

        if(!$color)
        	$color = array();

        $data = array('title_en' => $title_en, 'title_fr' => $title_fr,'desc_en' => $desc_en, 'desc_fr' => $desc_fr,'short_desc_en' => $short_desc_en, 'short_desc_fr' => $short_desc_fr, 'cat_id' => $cat_id, 'in_stock' => ($in_stock)?$in_stock:0,'due_stock' => $due_stock, 'price' => $price,'discount' => $discount, 'image' => $fileName,'route' => $route,'color' => json_encode($color), 'size' => $size, 'material' => $material,'weight' => $weight, 'is_most_selling' => ($is_most_selling)?1:0,'is_new_collection' => ($is_new_collection)?1:0);
        $this->products_model->updateProduct($productId,$data);
        $this->session->set_flashdata('message', 'inserted successfully');
		return redirect("dashboard/products", 'refresh');
	}

	public function add(){
		if($_POST){
			if (isset($_FILES) && count($_FILES) > 0){
				$config['upload_path']          = 'assets/images/site/products';
		        $config['allowed_types']        = 'gif|jpg|png|jpeg';

		        $this->load->library('upload', $config);

		        if ( ! $this->upload->do_upload('file'))
	                $this->session->set_flashdata('messageError', $this->upload->display_errors());
		        else{
		                $fileData =$this->upload->data();
	                $fileName = $fileData['file_name'];

	                $title_en = $this->input->post('title_en');
	                $title_fr = $this->input->post('title_fr');
	                $short_desc_en = $this->input->post('short_desc_en');
        			$short_desc_fr = $this->input->post('short_desc_fr');
	                $desc_en = $this->input->post('desc_en');
	                $desc_fr = $this->input->post('desc_fr');
	                $cat_id = $this->input->post('cat_id');
	                $in_stock = $this->input->post('in_stock');
	                $due_stock = $this->input->post('due_stock');
	                $price = $this->input->post('price');
	                $discount = $this->input->post('discount');
	                $route = $this->input->post('route');
	                $material = $this->input->post('material');
	                $weight = $this->input->post('weight');
	                $size = $this->input->post('size');
	                $color = $this->input->post('color');
	                $is_most_selling = $this->input->post('is_most_selling');
	                $is_new_collection = $this->input->post('is_new_collection');

	                $this->products_model->addProduct(array('title_en' => $title_en, 'title_fr' => $title_fr,'desc_en' => $desc_en, 'desc_fr' => $desc_fr,'short_desc_en' => $short_desc_en, 'short_desc_fr' => $short_desc_fr, 'cat_id' => $cat_id, 'in_stock' => ($in_stock)?$in_stock:0,'due_stock' =>$due_stock, 'price' => $price,'discount' => $discount, 'image' => $fileName,'route' => $route,'color' => json_encode($color), 'size' => $size, 'material' => $material,'weight' => $weight,'is_most_selling' => ($is_most_selling)?1:0,'is_new_collection' => ($is_new_collection)?1:0));


	                $this->session->set_flashdata('message', 'inserted successfully');
					redirect("dashboard/products", 'refresh');
		        }
			}
			else
                $this->session->set_flashdata('messageError', 'Image Required');

		}
		$this->data['categories'] = $this->categories_model->getAllCategories();
		$this->data['content'] = $this->load->view('dash/products/add',$this->data,TRUE);
		$this->load->view('dash/index',$this->data);
	}

	public function images($productId){

		if (isset($_FILES) && count($_FILES) > 0){
			for ($i=0; $i < count($_FILES['file']['name']); $i++) {
				$config['upload_path']          = 'assets/images/site/products';
		        $config['allowed_types']        = 'gif|jpg|png|jpeg';
		        $_FILES['fileOne']['name'] = $_FILES['file']['name'][$i];
	          	$_FILES['fileOne']['type'] = $_FILES['file']['type'][$i];
	          	$_FILES['fileOne']['tmp_name'] = $_FILES['file']['tmp_name'][$i];
	          	$_FILES['fileOne']['error'] = $_FILES['file']['error'][$i];
	          	$_FILES['fileOne']['size'] = $_FILES['file']['size'][$i];

		        $this->load->library('upload', $config);



		        if ($this->upload->do_upload('fileOne')){
	                $fileData =$this->upload->data();
	                $fileName = $fileData['file_name'];

	                $this->products_model->addProductImage($productId,$fileName);

		        }
			}
        	$this->session->set_flashdata('message', 'inserted successfully');
		}


		$images = $this->products_model->getProductImages($productId);
		$this->data['images'] = $images;
		$this->data['productId'] = $productId;
		// var_dump("ASD"); die();
		$this->data['content'] = $this->load->view('dash/products/images',$this->data,TRUE);
		$this->load->view('dash/index',$this->data);
	}

	public function deleteImage($productId,$imageId){
		$this->products_model->deleteImage($productId,$imageId);


        $this->session->set_flashdata('message', 'deleted successfully');
		redirect("dashboard/products/images/".$productId, 'refresh');

	}

	public function delete($productId){
		$this->products_model->deleteProduct($productId);

		$this->session->set_flashdata('message', 'removed successfully');
		redirect("dashboard/products", 'refresh');
	}
}
