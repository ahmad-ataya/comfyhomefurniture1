<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Categories extends MY_dash {

	public function __construct(){
	    parent::__construct();
	    $this->load->model('categories_model');
		$this->data['mainPage'] = $this->data['activePage'] = 'categories'; 
	}



	public function index($route = '')
	{
		$this->data['tableKeys'] = array('id' => 'ID','parent_title_en' => 'Parent Category' ,'title_en' => 'En Title','title_fr' => 'FR Title','image' => 'Imaeg','active' => 'active','actions' => 'Actions');
		$this->data['actionsValue'] = "<a href='".base_url()."dashboard/categories/edit/{ID}' class='btn btn-info' style='margin-right: 10px;'>Edit</a><a href='".base_url()."dashboard/categories/delete/{ID}' class='btn btn-danger'>Remove</a>";

		$this->data['tableData'] = $this->categories_model->getAllCategories();
		$this->data['keySpecific'] = array('image' => '<img  src="'.base_url().'assets/images/site/categories/{value}"   width="200">');
		$this->data['addPath'] = 'dashboard/categories/add';

		$this->data['content'] = $this->load->view('dash/table_view',$this->data,TRUE);
		$this->load->view('dash/index',$this->data);
	}

	public function edit($catId){
		$category = $this->categories_model->getAllCategories($catId);

		if(!$category)
			return redirect('dashboard/categories');

		$this->data['category'] = $category;
		
		$this->data['categories'] = $this->categories_model->getAllCategories();
		$this->data['content'] = $this->load->view('dash/categories/add',$this->data,TRUE);
		$this->load->view('dash/index',$this->data);
	}

	public function edit_save($catId){
		$category = $this->categories_model->getAllCategories($catId);

		if(!$category || !$_POST)
			return redirect('dashboard/categories');

		$fileName = $category->image;
		if (isset($_FILES) && count($_FILES) > 0){
			$config['upload_path']          = 'assets/images/site/categories';
	        $config['allowed_types']        = 'gif|jpg|png|jpeg';

	        $this->load->library('upload', $config);

	        if($this->upload->do_upload('file')){
	                $fileData =$this->upload->data();
                $fileName = $fileData['file_name'];

	        }
		}


        $title_en = $this->input->post('title_en');
        $title_fr = $this->input->post('title_fr');
        $parent_id = $this->input->post('parent_id');
        $active = $this->input->post('active');
        $route = $this->input->post('route');
        $data = array('title_en' => $title_en, 'title_fr' => $title_fr, 'parent_id' => $parent_id, 'active' => ($active)?1:0, 'route' => $route, 'image' => $fileName);
        $this->categories_model->updateCategory($catId,$data);
        $this->session->set_flashdata('message', 'inserted successfully');
		return redirect("dashboard/categories", 'refresh');
	}

	public function add(){
		if($_POST){
			if (isset($_FILES) && count($_FILES) > 0){
				$config['upload_path']          = 'assets/images/site/categories';
		        $config['allowed_types']        = 'gif|jpg|png|jpeg';

		        $this->load->library('upload', $config);

		        if ( ! $this->upload->do_upload('file'))
	                $this->session->set_flashdata('messageError', $this->upload->display_errors());
		        else{
		                $fileData =$this->upload->data();
	                $fileName = $fileData['file_name'];

	                $title_en = $this->input->post('title_en');
	                $title_fr = $this->input->post('title_fr');
	                $parent_id = $this->input->post('parent_id');
	                $active = $this->input->post('active');
	                $route = $this->input->post('route');

	                $this->categories_model->addCategory(array('title_en' => $title_en, 'title_fr' => $title_fr, 'parent_id' => $parent_id, 'active' => ($active)?1:0, 'route' => $route, 'image' => $fileName));

	                $this->session->set_flashdata('message', 'inserted successfully');
					redirect("dashboard/categories", 'refresh');
		        }
			}
			else
                $this->session->set_flashdata('messageError', 'Image Required');

		}
		$this->data['categories'] = $this->categories_model->getAllCategories();
		$this->data['content'] = $this->load->view('dash/categories/add',$this->data,TRUE);
		$this->load->view('dash/index',$this->data);
	}

	public function delete($catId){
		$this->categories_model->deleteCategory($catId);

		$this->session->set_flashdata('message', 'removed successfully');
		redirect("dashboard/categories", 'refresh');
	}
}
