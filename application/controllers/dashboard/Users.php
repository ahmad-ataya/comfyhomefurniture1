<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends MY_dash {

	public function __construct(){
	    parent::__construct();
	    $this->load->model('users_model');

		$this->data['mainPage'] = $this->data['activePage'] = 'users'; 
	}

	public function index($route = '')
	{
		$this->data['tableKeys'] = array('id' => 'ID','first_name' => 'First Name','last_name' => 'Last Name','email' => 'Email','last_login' => 'Last Login','actions' => 'Actions');
		$this->data['actionsValue'] = "<a href='".base_url()."dashboard/users/edit/{ID}' class='btn btn-info'>Edit</a>";
		$this->data['addPath'] = 'dashboard/users/add';
		
		$this->data['tableData'] = $this->users_model->getAllUsers();

		$this->data['content'] = $this->load->view('dash/table_view',$this->data,TRUE);
		$this->load->view('dash/index',$this->data);
	}

	public function edit($userId){
		$user = $this->users_model->getAllUsers($userId);

		if(!$user)
			return redirect('dashboard/users');

		$this->data['userData'] = $user;
		
		$this->data['content'] = $this->load->view('dash/users/add',$this->data,TRUE);
		$this->load->view('dash/index',$this->data);
	}

	public function add(){
		$this->data['content'] = $this->load->view('dash/users/add',$this->data,TRUE);
		$this->load->view('dash/index',$this->data);
	}
}
