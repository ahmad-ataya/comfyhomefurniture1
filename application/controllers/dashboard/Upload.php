<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Upload extends MY_dash {

	public function __construct(){
	    parent::__construct();
	    $this->load->model('global_model');
	}

	public function uploadImage()
	{
		$config['upload_path']          = '../assets/images/site/slider';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        // $config['max_size']             = 100;
        // $config['max_width']            = 1024;
        // $config['max_height']           = 768;

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('file'))
        {
                $this->session->set_flashdata('messageError', $this->upload->display_errors());
				// redirect('/dashboard/users/edit/'.$id, 'refresh');
        }
        else
        {
                echo array('upload_data' => $this->upload->data());

        }
	}

}
