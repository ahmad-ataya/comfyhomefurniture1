<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends MY_site {

	public function __construct(){
	    parent::__construct();
	    $this->load->model('products_model');
	}

	public function index($route = 0)
	{
	    $id = $route;
	    if(!is_numeric($route))
	        $id = 0;



		$product = $this->products_model->getProductInfo($id,$route);
		if(!$product)
			redirect('','refersh');

		$this->data['product'] = $product;
		$this->data['images'] = $this->products_model->getProductImages($product->id);

		$this->data['content'] = $this->load->view('site/product_view',$this->data,TRUE);
		$this->load->view('site/index',$this->data);
	}
}
