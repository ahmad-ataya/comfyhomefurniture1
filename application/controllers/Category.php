<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends MY_site {

	public function __construct(){
	    parent::__construct();
	    $this->load->model('categories_model');
	    $this->load->model('collections_model');
	    $this->load->model('products_model');
	}

	public function index($route = '')
	{
		$category = $this->categories_model->getCategory(false,$route);
		if(!$category)
			redirect('','refresh');
		$this->data['category'] = $category;
		$this->data['products'] = $this->products_model->getProductsOfCategory('',$category->id);
		
		$this->data['content'] = $this->load->view('site/category_view',$this->data,TRUE);
		$this->load->view('site/index',$this->data);
	}
}
